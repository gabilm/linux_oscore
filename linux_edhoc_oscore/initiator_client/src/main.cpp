/*
   Copyright (c) 2021 Fraunhofer AISEC. See the COPYRIGHT
   file at the top-level directory of this distribution.

   Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
   http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
   <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
   option. This file may not be copied, modified, or distributed
   except according to those terms.
*/

#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h> 
#include <netdb.h>

extern "C" {
#include "oscore.h"
#include "edhoc.h"
#include "sock.h"
#include "edhoc_test_vectors_p256_v16.h"
#include "oscore_test_vectors.h"
#include "inc/log.h"
#include "inc/utils.h"
}
#include "cantcoap.h"
#include "coap.h"

#include "libconfig.h"

#define USE_IPV4

struct context c_client;

uint8_t *c_x = NULL;
uint8_t *suites_x = NULL;
uint8_t *ead_a = NULL;
uint8_t *ead_b = NULL;
uint8_t *id_cred_x = NULL;
uint8_t *cred_x = NULL;
uint8_t *private_key  = NULL;
uint8_t *remote_id_cred_x = NULL;
uint8_t *remote_cred_x = NULL;

int num_queries = 1; 
int port = 5683;
char cfg_file [MAXLINE];


void cleanup() {
    free(suites_x); 
    free(c_x);
    free(ead_a);
    free(ead_b);
    free(id_cred_x);    
    free(cred_x);
    free(private_key);
    free(remote_id_cred_x);
    free(remote_cred_x);
}


/**
 * @brief   Initializes sockets for CoAP client.
 * @param
 * @retval  error code
 */
static int start_coap_client(int *sockfd, char* dst_host, int port) {
    int err;
    struct sockaddr_in servaddr;
    struct hostent *hoste = gethostbyname(dst_host);
    err = sock_init(SOCK_CLIENT, inet_ntoa(*(struct in_addr*)hoste->h_addr), IPv4, &servaddr,sizeof(servaddr), sockfd, port);
    if (err < 0) {
        P_ERR("error during socket initialization (error code: %d)",
               err);
        return -1;
    }
    return 0;
}

/**
 * @brief   Callback function called inside the frontend when data needs to 
 *      be send over the network. We use here CoAP as transport 
 * @param   data pointer to the data that needs to be send
 * @param   data_len lenhgt of the data in bytes
 */
enum err tx(void *sock, uint8_t *data, uint32_t data_len)
{
    /*construct a CoAP packet*/
    static uint16_t mid = 0;
    static uint32_t token = 0;
    CoapPDU *pdu = new CoapPDU();
    pdu->reset();
    pdu->setVersion(1);
    pdu->setType(CoapPDU::COAP_CONFIRMABLE);
    pdu->setCode(CoapPDU::COAP_POST);
    pdu->setToken((uint8_t *)&(++token), sizeof(token));
    pdu->setMessageID(mid++);
    pdu->setURI((char *)".well-known/edhoc", 17);
    pdu->setPayload(data, data_len);

    send(*(int *)sock, pdu->getPDUPointer(), pdu->getPDULength(), 0);

    delete pdu;
    return ok;
}

/**
 * @brief   Callback function called inside the frontend when data needs to 
 *      be received over the network. We use here CoAP as transport 
 * @param   data pointer to the data that needs to be received
 * @param   data_len lenhgt of the data in bytes
 */
enum err rx(void *sock, uint8_t *data, uint32_t *data_len)
{
    int n;
    char buffer[MAXLINE];
    CoapPDU *recvPDU;
    /* receive */
    n = recv(*(int *)sock, (char *)buffer, MAXLINE, MSG_WAITALL);
    if (n < 0) {
        P_ERR("recv error");
    }

    recvPDU = new CoapPDU((uint8_t *)buffer, n);

    if (recvPDU->validate()) {
        recvPDU->printHuman();
    }

    uint32_t payload_len = recvPDU->getPayloadLength();
    P_INFO("data_len: %d", *data_len);
    P_INFO("payload_len: %d", payload_len);

    if (*data_len >= payload_len) {
        memcpy(data, recvPDU->getPayloadPointer(), payload_len);
        *data_len = payload_len;
    } else {
        P_ERR("insufficient space in buffer");
        return buffer_to_small;
    }

    delete recvPDU;
    return ok;
}

int 
main(int argc, char **argv) {
    
    // Get options
    int c;
    strcpy(cfg_file, "/etc/edhoc_oscore.cfg");
    
    while ( ( c = getopt ( argc, argv, "c:n:v:h" ) ) != -1 ) {
        switch ( c ) {
            case 'v':
                if (strcmp(optarg,"0") != 0 && strcmp(optarg,"1") != 0 && strcmp(optarg,"2") != 0) {
                    printf("verbose not valid: %s",optarg);
                    exit(EXIT_FAILURE);
                } else if (strcmp(optarg,"0") == 0) {
                    set_verb_level(CI_VERB_ERROR_);
                } else if (strcmp(optarg,"2") == 0) {
                    set_verb_level(CI_VERB_DEBUG_);
                }
                break;
            case 'n': {
                num_queries = atoi(optarg);
                if (num_queries > 0)
                    printf("num_queries: %d\n",num_queries);        
                else {
                    printf("num_queries %d invalid\n",num_queries);
                    return 0;
                }
                break;
            }   
            case 'c': {
                printf("configuration file: %s\n", optarg);
                strcpy(cfg_file, optarg);
                break;
            }
            case 'h': {
                fprintf(stderr, "Usage: %s [-c conf file (default /etc/edhoc_oscore.cfg)] [-n num_queries (default 1)] [-v verbose_level]", argv[0]);
                return 0;
            }
            default: {
                fprintf(stderr, "Usage: %s [-c conf file (default /etc/edhoc_oscore.cfg)] [-n num_queries (default 1)] [-v verbose_level]", argv[0]);
                exit(EXIT_FAILURE);
            }
        }
    }

    int sockfd;
    uint8_t oscore_master_secret[16];
    uint8_t oscore_master_salt[8];

    /* edhoc declarations */
    uint8_t PRK_out[PRK_DEFAULT_SIZE];
    uint8_t prk_exporter[32];
    uint8_t err_msg[ERR_MSG_DEFAULT_SIZE];
    uint32_t err_msg_len = sizeof(err_msg);
    uint8_t ad_2[AD_DEFAULT_SIZE];
    uint32_t ad_2_len = sizeof(ad_2);
    uint8_t ad_4[AD_DEFAULT_SIZE];
    uint32_t ad_4_len = sizeof(ad_2);

    uint16_t cred_num = 1;
    struct other_party_cred cred_r;
    struct edhoc_initiator_context c_i;

    /* ******************************* */
    /* BEGIN Reading configuration file */
    /* ******************************* */
    // This code reads from the /etc/edhoc_oscore.cfg file. For testing purposes only reads one
    // entry for sections auth_entries and connections.
    // this should be moved to a function out of main
    
    //auth_entries
    const char *auth_entry_name = NULL;
    int id_cred_x_l = 0;
    int method = 0;
    int cred_x_l = 0;
    int private_key_l = 0;    
    //connections
    const char *conn_name = NULL;
    int local_autostartup = 0;
    const char *local_auth_cred_ref = NULL;
    int c_x_l = 0;
    int suites_x_l = 0;
    int ead_a_l = 0;
    int ead_b_l = 0;
    int remote_id_cred_x_l = 0;
    const char *remote_auth_method = NULL;
    int remote_cred_x_l = 0;
    bool key_confirmation = false;
    int set_oscore = 1;
    
    //libconfig 
    config_t cfg, *cf;
    const config_setting_t *connections;
    const config_setting_t *connection;
    const config_setting_t *auth_entries;
    const config_setting_t *auth_entry;
    const config_setting_t *conn_local;
    const config_setting_t *conn_remote;
    int count = 0; 
    int i =0;

    cf = &cfg;
    config_init(cf);

    if (!config_read_file(cf, cfg_file)) {
        P_ERR("Can't open %s %s:%d - %s",
            cfg_file,
            config_error_file(cf),
            config_error_line(cf),
            config_error_text(cf));
        config_destroy(cf);
        return(EXIT_FAILURE);
    }

    // read auth_entry section 
    auth_entries = config_lookup(cf, "auth-entry");
    if (auth_entries != NULL){
        count = config_setting_length(auth_entries);
        P_DBG("I have %i auth_entries:", count);
        for (i = 0; i < count; i++) {
            auth_entry = config_setting_get_elem(auth_entries, i);          
            config_setting_lookup_string(auth_entry,"name",&auth_entry_name);
            P_DBG("auth-entry name: %s", auth_entry_name);  
            
			const char *id_cred_x_b64 = NULL;
            if (config_setting_lookup_string(auth_entry, "id-cred-x", &id_cred_x_b64)) {
                P_DBG("auth-entry id-cred-x base64: %s", id_cred_x_b64);
				size_t out_len;
				id_cred_x = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
				if (!mbedtls_base64_decode(id_cred_x,MAX_LEN,&out_len,(unsigned char *)id_cred_x_b64, strlen(id_cred_x_b64))) {
					id_cred_x_l  = (int)out_len;
					P_DBG("id-cred-x len: %i",id_cred_x_l);
				} else P_ERR("Error decoding id-cred-x ");	
            } else 
                P_DBG("auth-entry id-cred-x not defined");
            
            const char *auth_method = NULL;
            if (config_setting_lookup_string(auth_entry, "auth-method", &auth_method)) {
                P_DBG("auth-entry auth-method: %s", auth_method);
                if (!strcmp(auth_method,"static-dh-key")) method = 3;
            } else 
                P_DBG("auth-entry auth_method not defined, using default");
        
			const char *cred_x_b64 = NULL;
            if (config_setting_lookup_string(auth_entry, "cred-x", &cred_x_b64)) {
                P_DBG("auth-entry cred-x base64: %s", cred_x_b64);
				size_t out_len;
				cred_x = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
				if (!mbedtls_base64_decode(cred_x,MAX_LEN,&out_len,(unsigned char *)cred_x_b64, strlen(cred_x_b64))) {
					cred_x_l  = (int)out_len;
					P_DBG("cred-x len: %i",cred_x_l);
				} else P_ERR("Error decoding cred-x ");	
            } else 
                P_DBG("auth-entry cred-x not defined");
			          
			const char *private_key_b64 = NULL;
            if (config_setting_lookup_string(auth_entry, "private-key", &private_key_b64)) {
                P_DBG("auth-entry private-key base64: %s", private_key_b64);
				size_t out_len;
				private_key = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
				if (!mbedtls_base64_decode(private_key,MAX_LEN,&out_len,(unsigned char *)private_key_b64, strlen(private_key_b64))) {
					private_key_l  = (int)out_len;
					P_DBG("private-key len: %i",private_key_l);
				} else P_ERR("Error decoding private-key ");	
            } else 
                P_DBG("auth-entry private-key not defined");    
        }
    }
    
    // read connection section
    connections = config_lookup(cf, "connection");
    if (connections != NULL){
        count = config_setting_length(connections);
        P_DBG("I have %i connections:", count);
        for (i = 0; i < count; i++) {
            connection = config_setting_get_elem(connections, i);       
            config_setting_lookup_string(connection,"name",&conn_name);
            P_DBG("connectio _name: %s", conn_name);    
            int key_c = 0;
            if (config_setting_lookup_bool(connection, "key-confirmation", &key_c)) {
                P_DBG("connection key-confirmation: %i", key_c);
                if (key_c == 1) key_confirmation = true;
            } else 
                P_DBG("connection key-confirmation not defined");
            
            if (config_setting_lookup_bool(connection, "set-oscore", &set_oscore)) {
                P_DBG("connection set-oscore: %i", set_oscore);
            } else 
                P_DBG("connection set-oscore not defined");
            
            conn_local = config_setting_get_member (connection,"local");
            if (conn_local != NULL) {           
                if (config_setting_lookup_bool(conn_local, "autostartup", &local_autostartup)) {
                    P_DBG("local autostartup: %i", local_autostartup);
                }
                else
                    P_DBG("local autostartup not defined, using default");
                if (config_setting_lookup_string(conn_local, "auth-cred-ref", &local_auth_cred_ref)) {
                    P_DBG("local auth-cred-ref: %s", local_auth_cred_ref);
                } else 
                    P_DBG("local id-cred-x-ref not defined");   
				
				
				const char *local_suites_x_b64 = NULL;
			    if (config_setting_lookup_string(conn_local, "suites-x", &local_suites_x_b64)) {
                    P_DBG("local suites-x base54: %s", local_suites_x_b64);
					size_t out_len;
					suites_x = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
					if (!mbedtls_base64_decode(suites_x,MAX_LEN,&out_len,(unsigned char *)local_suites_x_b64, strlen(local_suites_x_b64))) {
						suites_x_l  = (int)out_len;
						P_DBG("local-suites-x len: %i",suites_x_l);
					} else P_ERR("Error decoding suites-x");	
                } else 
                    P_DBG("local suites_x not defined");   
				 
				
				const char *c_x_b64 = NULL; 
                if (config_setting_lookup_string(conn_local, "c-x", &c_x_b64)) {
                    P_DBG("c-x base64: %s", c_x_b64);
					size_t out_len;
					c_x = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
					if (!mbedtls_base64_decode(c_x,MAX_LEN,&out_len,(unsigned char *)c_x_b64, strlen(c_x_b64))) {
						c_x_l  = (int)out_len;
						P_DBG("c-x len: %i",c_x_l);
					} else P_ERR("Error decoding c-x ");	
                } else 
                    P_DBG("c-x not defined");  
			
				const char *ead_a_b64 = NULL;  
                if (config_setting_lookup_string(conn_local, "ead-a", &ead_a_b64)) {
                    P_DBG("ead-a base64: %s", ead_a_b64);
					size_t out_len;
					ead_a = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
					if (!mbedtls_base64_decode(ead_a,MAX_LEN,&out_len,(unsigned char *)ead_a_b64, strlen(ead_a_b64))) {
						ead_a_l  = (int)out_len;
						P_DBG("ead-a len: %i",ead_a_l);
					} else P_ERR("Error decoding ead-a");	
                } else 
                    P_DBG("local ead-a not defined"); 
				
				const char *ead_b_b64 = NULL;  
                if (config_setting_lookup_string(conn_local, "ead-b", &ead_b_b64)) {
                    P_DBG("ead-b base64: %s", ead_b_b64);
					size_t out_len;
					ead_b = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
					if (!mbedtls_base64_decode(ead_b,MAX_LEN,&out_len,(unsigned char *)ead_b_b64, strlen(ead_b_b64))) {
						ead_b_l  = (int)out_len;
						P_DBG("ead-b len: %i",ead_b_l);
					} else P_ERR("Error decoding ead-b");	
                } else 
                    P_DBG("local ead-b not defined");                   
            }
            conn_remote = config_setting_get_member (connection,"remote");
            if (conn_remote != NULL) { 
            
				const char *remote_id_cred_x_b64 = NULL;
				if (config_setting_lookup_string(conn_remote, "id-cred-x", &remote_id_cred_x_b64)) {
                    P_DBG("remote id_cred-x base64: %s", remote_id_cred_x_b64);
					size_t out_len;
					remote_id_cred_x = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
					if (!mbedtls_base64_decode(remote_id_cred_x,MAX_LEN,&out_len,(unsigned char *)remote_id_cred_x_b64, strlen(remote_id_cred_x_b64))) {
						remote_id_cred_x_l  = (int)out_len;
						P_DBG("remote id-cred-x len: %i",remote_id_cred_x_l);
					} else P_ERR("Error decoding remote id-cred-x ");	
                } else 
                    P_DBG("remote id-cred-x not defined");  
				
				
                if (config_setting_lookup_string(conn_remote, "auth-method", &remote_auth_method)) {
                    P_DBG("remote auth-method: %s", remote_auth_method);
                } else 
                    P_DBG("remote auth-method not defined"); 
				
				const char *remote_cred_x_b64 = NULL;   
                if (config_setting_lookup_string(conn_remote, "cred-x", &remote_cred_x_b64)) {
                    P_DBG("remote cred-x base64: %s", remote_cred_x_b64);
					size_t out_len;
					remote_cred_x = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
					if (!mbedtls_base64_decode(remote_cred_x,MAX_LEN,&out_len,(unsigned char *)remote_cred_x_b64, strlen(remote_cred_x_b64))) {
						remote_cred_x_l  = (int)out_len;
						P_DBG("remote-cred-x len: %i",remote_cred_x_l);
					} else P_ERR("Error decoding remote-cred-x ");	
                } else 
                    P_DBG("remote cred-x not defined");                 
            }   
        }       
    }

    // read target-resource list
    const config_setting_t *target_resources;
    const config_setting_t *target_resource;
    const char *target_uri = NULL;
    const char *target_policy = NULL;
    const char *target_ref = NULL;
    target_resources = config_lookup(cf, "target-resource");
    if (target_resources != NULL){
        // num of contexts 
        count = config_setting_length(target_resources);
        P_DBG("I have %i target-resources:", count);
        
        //for each target read elements 
        //WARNING: just one target is read, more target entries
        for (i = 0; i < count; i++) {
            target_resource = config_setting_get_elem(target_resources, i);
            config_setting_lookup_string(target_resource,"target",&target_uri);
            P_DBG("target-resource uri: %s", target_uri);   
            if (target_uri == NULL) {
                P_ERR("target-resource URI not defined");
                cleanup();
                return 0;
            }
            config_setting_lookup_string(target_resource,"policy",&target_policy);
            P_DBG("target-resource policy: %s", target_policy);
            
            config_setting_lookup_string(target_resource,"conn-ref",&target_ref);
            P_DBG("target-resource context ref: %s", target_ref);
        }
    }
    else {
        ERR("target resources not defined.");
        config_destroy(cf);
        cleanup();
        return 0;
    }
    
    coap_uri_t coap_uri;  
    int result = coap_split_uri((unsigned char *)target_uri, strlen(target_uri), &coap_uri);
    if (result != 0) {
        P_ERR("URI parse error");
        config_destroy(cf);
        cleanup();
        return 0;
    }
    char host [MAXLINE];
    strncpy(host,(char *)coap_uri.host.s,coap_uri.host.length);
    host[coap_uri.host.length] = '\0';
    char path [MAXLINE];
    strncpy(path,(char *)coap_uri.path.s,coap_uri.path.length);
    path[coap_uri.path.length] = '\0';
    P_DBG("URI host name: %s",host);
    P_DBG("URI port: %i",coap_uri.port); 
    P_DBG("URI path: %s",path);
    
    if (strlen(host) == 0){
        P_ERR("Host server not defined in URI");
        config_destroy(cf);
        cleanup();
        return 0;
    }
    if (strlen(path) == 0){
        P_DBG("Path not defined in URI");
        strcpy(path,"/");
    }
    if (coap_uri.port == 0){
        coap_uri.port=port;
    }
    /* ****************************** */
    /* END Reading configuration file */
    /* ****************************** */
    
    
    // Init CoAP client connection
    TRY_EXPECT(start_coap_client(&sockfd, host, coap_uri.port), 0);

    
    
    /* ****************************** */
    /* BEGIN Init EDHOC parameters    */
    /* ****************************** */
    // move to a function
    
    //conn_i
    c_i.c_i.len = c_x_l;
    c_i.c_i.ptr = (uint8_t *)c_x;
    // key_confirmation
    c_i.msg4 = key_confirmation;
    c_i.method = (enum method_type)method;
    // suites_i
    c_i.suites_i.len = suites_x_l;
    c_i.suites_i.ptr = (uint8_t *)suites_x;
    // ead_1
    c_i.ead_1.len = ead_a_l;
    c_i.ead_1.ptr = (uint8_t *)ead_a;
    //ead_3
    c_i.ead_3.len = ead_b_l;
    c_i.ead_3.ptr = (uint8_t *)ead_b;
    //id_cred_i
    c_i.id_cred_i.len = id_cred_x_l;
    c_i.id_cred_i.ptr = (uint8_t *)id_cred_x;
    // cred_i
    c_i.cred_i.len = cred_x_l;
    c_i.cred_i.ptr = (uint8_t *)cred_x;
    // from test_vector_p256_v16 vec_num = 1 g_x_raw and x_raw. They should be generated in realtime    
    const uint8_t g_x [] = {0x8a, 0xf6, 0xf4, 0x30, 0xeb, 0xe1, 0x8d, 0x34, 0x18, 0x40, 0x17, 0xa9, 0xa1, 0x1b, 0xf5, 0x11, 0xc8, 0xdf, 0xf8, 0xf8, 0x34, 0x73, 0x0b, 0x96, 0xc1, 0xb7, 0xc8, 0xdb, 0xca, 0x2f, 0xc3, 0xb6};
    const uint8_t x[] = {0x36, 0x8e, 0xc1, 0xf6, 0x9a, 0xeb, 0x65, 0x9b, 0xa3, 0x7d, 0x5a, 0x8d, 0x45, 0xb2, 0x1b, 0xdc, 0x02, 0x99, 0xdc, 0xea, 0xa8, 0xef, 0x23, 0x5f, 0x3c, 0xa4, 0x2c, 0xe3, 0x53, 0x0f, 0x95, 0x25};
    c_i.g_x.len = sizeof(g_x);
    c_i.g_x.ptr = (uint8_t *)g_x;
    c_i.x.len = sizeof(x);
    c_i.x.ptr = (uint8_t *)x;
    
    // privake_key
    if (c_i.method == INITIATOR_SK_RESPONDER_SK) {
        c_i.sk_i.len = private_key_l;
        c_i.sk_i.ptr = (uint8_t *)private_key;
    } else {    
        // static DH keys
        // from test_vector_p256_v16 vec_num = 5 g_i_raw. It has to be extracted from cred_i. 
        const uint8_t g_i[] = {0xac, 0x75, 0xe9, 0xec, 0xe3, 0xe5, 0x0b, 0xfc, 0x8e, 0xd6, 0x03, 0x99, 0x88, 0x95, 0x22, 0x40, 0x5c, 0x47, 0xbf, 0x16, 0xdf, 0x96, 0x66, 0x0a, 0x41, 0x29, 0x8c, 0xb4, 0x30, 0x7f, 0x7e, 0xb6};
        c_i.g_i.len = sizeof(g_i);
        c_i.g_i.ptr = (uint8_t *)g_i;
        c_i.i.len = private_key_l;
        c_i.i.ptr = (uint8_t *)private_key;
    }
    
    c_i.sock = &sockfd;

    // id_cred_r
    cred_r.id_cred.len = remote_id_cred_x_l;
    cred_r.id_cred.ptr = (uint8_t *)remote_id_cred_x;
    //cred_r
    cred_r.cred.len = remote_cred_x_l;
    cred_r.cred.ptr = remote_cred_x;

    if (c_i.method == INITIATOR_SK_RESPONDER_SK) {
        // from test_vector_p256_v16 vec_num = 1 ca_r and ca_pk. ca_r is not in cred_r!!
        const uint8_t ca_r[] = {0x30, 0x82, 0x01, 0x18, 0x30, 0x81, 0xc0, 0xa0, 0x03, 0x02, 0x01, 0x02, 0x02, 0x04, 0x61, 0xe9, 0x97, 0xc5, 0x30, 0x0a, 0x06, 0x08, 0x2a, 0x86, 0x48, 0xce, 0x3d, 0x04, 0x03, 0x02, 0x30, 0x15, 0x31, 0x13, 0x30, 0x11, 0x06, 0x03, 0x55, 0x04, 0x03, 0x0c, 0x0a, 0x45, 0x44, 0x48, 0x4f, 0x43, 0x20, 0x52, 0x6f, 0x6f, 0x74, 0x30, 0x1e, 0x17, 0x0d, 0x32, 0x32, 0x30, 0x31, 0x32, 0x30, 0x31, 0x37, 0x31, 0x31, 0x33, 0x33, 0x5a, 0x17, 0x0d, 0x32, 0x39, 0x31, 0x32, 0x33, 0x31, 0x32, 0x33, 0x30, 0x30, 0x30, 0x30, 0x5a, 0x30, 0x15, 0x31, 0x13, 0x30, 0x11, 0x06, 0x03, 0x55, 0x04, 0x03, 0x0c, 0x0a, 0x45, 0x44, 0x48, 0x4f, 0x43, 0x20, 0x52, 0x6f, 0x6f, 0x74, 0x30, 0x59, 0x30, 0x13, 0x06, 0x07, 0x2a, 0x86, 0x48, 0xce, 0x3d, 0x02, 0x01, 0x06, 0x08, 0x2a, 0x86, 0x48, 0xce, 0x3d, 0x03, 0x01, 0x07, 0x03, 0x42, 0x00, 0x04, 0x27, 0xec, 0xf4, 0xb4, 0x66, 0xd3, 0xcd, 0x61, 0x14, 0x4c, 0x94, 0x40, 0x21, 0x83, 0x8d, 0x57, 0xbf, 0x67, 0x01, 0x97, 0x33, 0x78, 0xa1, 0x5b, 0x3f, 0x5d, 0x27, 0x57, 0x5d, 0x34, 0xc4, 0xa9, 0x7b, 0x79, 0xe0, 0xf2, 0x4b, 0x44, 0x6b, 0xca, 0x67, 0xe1, 0x3d, 0x75, 0xd0, 0x95, 0x73, 0x12, 0x4b, 0x49, 0xb8, 0x38, 0xb1, 0x09, 0x73, 0xf0, 0xfb, 0x67, 0xe1, 0x26, 0x05, 0x1c, 0x95, 0x95, 0x30, 0x0a, 0x06, 0x08, 0x2a, 0x86, 0x48, 0xce, 0x3d, 0x04, 0x03, 0x02, 0x03, 0x47, 0x00, 0x30, 0x44, 0x02, 0x20, 0x13, 0x73, 0x43, 0x26, 0xf2, 0xca, 0x35, 0xd1, 0xae, 0xdb, 0x6d, 0x5e, 0x1c, 0x8e, 0xb7, 0xb9, 0x65, 0xda, 0x67, 0xea, 0xd3, 0x31, 0x4e, 0x50, 0x29, 0x09, 0xb9, 0xd7, 0x57, 0xcb, 0xa1, 0x68, 0x02, 0x20, 0x49, 0xba, 0x0b, 0xa4, 0xf0, 0x6e, 0xfe, 0x8c, 0x0d, 0x9c, 0x3d, 0x31, 0x15, 0xeb, 0x9c, 0x96, 0xca, 0x46, 0xd1, 0x28, 0x49, 0x9b, 0x68, 0x95, 0x7d, 0x0a, 0x85, 0xaf, 0x13, 0x6b, 0xf3, 0x06};
        // at least ca_pk should be extracted from ca
        const uint8_t ca_r_pk[] = {0x04, 0x27, 0xec, 0xf4, 0xb4, 0x66, 0xd3, 0xcd, 0x61, 0x14, 0x4c, 0x94, 0x40, 0x21, 0x83, 0x8d, 0x57, 0xbf, 0x67, 0x01, 0x97, 0x33, 0x78, 0xa1, 0x5b, 0x3f, 0x5d, 0x27, 0x57, 0x5d, 0x34, 0xc4, 0xa9, 0x7b, 0x79, 0xe0, 0xf2, 0x4b, 0x44, 0x6b, 0xca, 0x67, 0xe1, 0x3d, 0x75, 0xd0, 0x95, 0x73, 0x12, 0x4b, 0x49, 0xb8, 0x38, 0xb1, 0x09, 0x73, 0xf0, 0xfb, 0x67, 0xe1, 0x26, 0x05, 0x1c, 0x95, 0x95};
        cred_r.ca.len = sizeof(ca_r);
        cred_r.ca.ptr = (uint8_t *)ca_r;
        cred_r.ca_pk.len = sizeof(ca_r_pk);
        cred_r.ca_pk.ptr = (uint8_t *)ca_r_pk;
    } else {
        // from test_vector_p256_v16 vec_num = 5 g_r_raw. It has to be extracted from cred_r.cred.ptr
        const uint8_t g[] = {0xbb, 0xc3, 0x49, 0x60, 0x52, 0x6e, 0xa4, 0xd3, 0x2e, 0x94, 0x0c, 0xad, 0x2a, 0x23, 0x41, 0x48, 0xdd, 0xc2, 0x17, 0x91, 0xa1, 0x2a, 0xfb, 0xcb, 0xac, 0x93, 0x62, 0x20, 0x46, 0xdd, 0x44, 0xf0};
        cred_r.g.len = sizeof(g);
        cred_r.g.ptr = (uint8_t *)g;  
    }
    
    /* ****************************** */
    /* END Init EDHOC parameters    */
    /* ****************************** */    
    
    
    
    /* *********************************************************** */
    /* BEGIN EDHOC negotiation and OSCORE context derivation       */
    /* *********************************************************** */   
    TRY(edhoc_initiator_run(&c_i, &cred_r, cred_num, err_msg, &err_msg_len,
                ad_2, &ad_2_len, ad_4, &ad_4_len, PRK_out,
                sizeof(PRK_out), tx, rx));

    PRINT_ARRAY("PRK_out", PRK_out, sizeof(PRK_out));

    TRY(prk_out2exporter(SHA_256, PRK_out, sizeof(PRK_out), prk_exporter));
    PRINT_ARRAY("prk_exporter", prk_exporter, sizeof(prk_exporter));

    TRY(edhoc_exporter(SHA_256, OSCORE_MASTER_SECRET, prk_exporter,
               sizeof(prk_exporter), oscore_master_secret,
               sizeof(oscore_master_secret)));
    PRINT_ARRAY("OSCORE Master Secret", oscore_master_secret,
            sizeof(oscore_master_secret));

    TRY(edhoc_exporter(SHA_256, OSCORE_MASTER_SALT, prk_exporter,
               sizeof(prk_exporter), oscore_master_salt,
               sizeof(oscore_master_salt)));
    PRINT_ARRAY("OSCORE Master Salt", oscore_master_salt,
            sizeof(oscore_master_salt));
            
    /*OSCORE contex initialization*/
    oscore_init_params params = {
        CLIENT,
        sizeof(oscore_master_secret),
        oscore_master_secret,
        T1__SENDER_ID_LEN,
        (uint8_t *)T1__SENDER_ID,
        T1__RECIPIENT_ID_LEN,
        (uint8_t *)T1__RECIPIENT_ID,
        T1__ID_CONTEXT_LEN,
        (uint8_t *)T1__ID_CONTEXT,
        sizeof(oscore_master_salt),
        oscore_master_salt,
        OSCORE_AES_CCM_16_64_128,
        OSCORE_SHA_256,
    };
    TRY(oscore_context_init(&params, &c_client));       
    
    /* *********************************************************** */
    /* END EDHOC negotiation and OSCORE context derivation       */
    /* *********************************************************** */   
    
    
    /* *********************************************************** */
    /* BEGIN CoAP+OSCORE exchange                                  */
    /* *********************************************************** */   

    char buffer[MAXLINE];
    /*construct a CoAP packet*/
    uint16_t mid1 = 256, mid2 = 0;
    uint32_t token = 0;
    int32_t n;
    bool oscore_flag = false;
    CoapPDU *unprotected_pdu = new CoapPDU();
    CoapPDU *protected_pdu = new CoapPDU();

    uint8_t buf_oscore[256];
    uint32_t buf_oscore_len = sizeof(buf_oscore);
    uint8_t coap_rx_buf[256];
    uint32_t coap_rx_buf_len = 0;
    CoapPDU *recvPDU;
    bool request_secure_resource = true;
    uint8_t request_payload[] = { "This is some payload" };
    
    
    //check policy 
    if (!strcmp(target_policy,"bypass")) {
        request_secure_resource = false; 
    } else if (!strcmp(target_policy,"discard")) {
        P_INFO("Target resource discarded");
        config_destroy(cf);
        cleanup();
        return 0;
    }

    int nq = 0;
    while (nq < num_queries) {
        nq++;
        if (request_secure_resource) {
            /* send OSCORE request*/
            protected_pdu->reset();
            protected_pdu->setVersion(1);
            protected_pdu->setType(CoapPDU::COAP_CONFIRMABLE);
            protected_pdu->setCode(CoapPDU::COAP_GET);
            protected_pdu->setToken((uint8_t *)&(++token),
                        sizeof(token));
            protected_pdu->setURI((char *)path, strlen(path));
            protected_pdu->setMessageID(mid1++);
            protected_pdu->setPayload(request_payload,
                          sizeof(request_payload));

            if (protected_pdu->validate()) {
                P_INFO("=================================================");
                P_INFO("CoAP message to be protected with OSCORE");
                protected_pdu->printHuman();
            }

            TRY(coap2oscore(protected_pdu->getPDUPointer(),
                    (uint16_t)protected_pdu->getPDULength(),
                    buf_oscore, &buf_oscore_len,
                    &c_client));

            send(sockfd, buf_oscore, buf_oscore_len, 0);
            /* receive */
            n = recv(sockfd, (char *)buffer, MAXLINE, MSG_WAITALL);

            if (n < 0) {
                P_INFO("no response received");
            } else {
                TRY(oscore2coap((uint8_t *)buffer, n,
                        coap_rx_buf, &coap_rx_buf_len,
                        &oscore_flag, &c_client));

                recvPDU = new CoapPDU((uint8_t *)coap_rx_buf,
                              coap_rx_buf_len);
                if (recvPDU->validate()) {
                    P_INFO("===================================================");
                    P_INFO("OSCORE message received and converted to CoAP:");
                    recvPDU->printHuman();
                }
            }

        } else {
            /* send CoAP request*/
            unprotected_pdu->reset();
            unprotected_pdu->setVersion(1);
            unprotected_pdu->setType(CoapPDU::COAP_CONFIRMABLE);
            unprotected_pdu->setCode(CoapPDU::COAP_GET);
            unprotected_pdu->setToken((uint8_t *)&(++token),
                          sizeof(token));
            unprotected_pdu->setURI((char *)path, strlen(path));
            unprotected_pdu->setMessageID(mid2++);
            unprotected_pdu->setPayload(request_payload,
                            sizeof(request_payload));

            if (unprotected_pdu->validate()) {
                P_INFO("=================================================");
                P_INFO("Unprotected CoAP message");
                unprotected_pdu->printHuman();
            }

            send(sockfd, unprotected_pdu->getPDUPointer(),
                 unprotected_pdu->getPDULength(), 0);
            /* receive */
            n = recv(sockfd, (char *)buffer, MAXLINE, MSG_WAITALL);

            if (n < 0) {
                P_INFO("no response received");
            } else {
                recvPDU = new CoapPDU((uint8_t *)buffer, n);
                if (recvPDU->validate()) {
                    P_INFO("=============================================");
                    P_INFO("Unprotected CoAP response message");
                    recvPDU->printHuman();
                }
            }
        }
        /*wait 5 sec before sending the next packet*/
        sleep(5);
    }
    
    /* *********************************************************** */
    /* END CoAP+OSCORE exchange                                  */
    /* *********************************************************** */
    
    
    close(sockfd);
    cleanup();  
    return 0;
}
