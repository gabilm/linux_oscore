/*
   Copyright (c) 2021 Fraunhofer AISEC. See the COPYRIGHT
   file at the top-level directory of this distribution.

   Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
   http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
   <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
   option. This file may not be copied, modified, or distributed
   except according to those terms.
*/

#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <netdb.h>

extern "C" {
#include "oscore.h"
#include "edhoc.h"
#include "sock.h"
#include "edhoc_test_vectors_p256_v16.h"
#include "oscore_test_vectors.h"
#include "inc/log.h"
#include "inc/utils.h"
}
#include "cantcoap.h"
#include "coap.h"
#include "libconfig.h"

#define USE_IPV4

CoapPDU *txPDU = new CoapPDU();

char buffer[MAXLINE];
CoapPDU *rxPDU;

#ifdef USE_IPV6
struct sockaddr_in6 client_addr;
#endif
#ifdef USE_IPV4
struct sockaddr_in client_addr;
#endif
socklen_t client_addr_len;

uint8_t *c_x = NULL;
uint8_t *suites_x = NULL;
uint8_t *ead_a = NULL;
uint8_t *ead_b = NULL;
uint8_t *id_cred_x = NULL;
uint8_t *cred_x = NULL;
uint8_t *private_key  = NULL;
uint8_t *remote_id_cred_x = NULL;
uint8_t *remote_cred_x = NULL;

int port = 5683;
char cfg_file [MAXLINE];

void cleanup() {
    free(suites_x); 
    free(c_x);
    free(ead_a);
    free(ead_b);
    free(id_cred_x);    
    free(cred_x);
    free(private_key);
    free(remote_id_cred_x);
    free(remote_cred_x);
}

/**
 * @brief   Initializes socket for CoAP server.
 * @param   
 * @retval  error code
 */
static int start_coap_server(int *sockfd, char *dst_host, int port)
{
    int err;
    struct sockaddr_in servaddr;
    //struct sockaddr_in client_addr;
    client_addr_len = sizeof(client_addr);
    memset(&client_addr, 0, sizeof(client_addr));
    //const char IPV4_SERVADDR[] = { "0.0.0.0" };
    struct hostent *hoste = gethostbyname(dst_host);
    P_DBG("ip address: %s", inet_ntoa(*(struct in_addr*)hoste->h_addr));
    err = sock_init(SOCK_SERVER, inet_ntoa(*(struct in_addr*)hoste->h_addr), IPv4, &servaddr,sizeof(servaddr), sockfd, port);
    if (err < 0) {
        P_ERR("error during socket initialization (error code: %d)",
               err);
        return -1;
    }
    return 0;
}
/**
 * @brief   Sends CoAP packet over network.
 * @param   pdu pointer to CoAP packet
 * @retval  error code
 */
static int send_coap_reply(void *sock, CoapPDU *pdu)
{
    int r;

    r = sendto(*(int *)sock, pdu->getPDUPointer(), pdu->getPDULength(), 0,
           (struct sockaddr *)&client_addr, client_addr_len);
    if (r < 0) {
        P_ERR("Error: failed to send reply (Code: %d, ErrNo: %d)", r,
               errno);
        return r;
    }
    //printf("CoAP reply sent!\n");
    return 0;
}

enum err tx(void *sock, uint8_t *data, uint32_t data_len)
{
    txPDU->setCode(CoapPDU::COAP_CHANGED);
    txPDU->setPayload(data, data_len);
    send_coap_reply(sock, txPDU);
    return ok;
}

enum err rx(void *sock, uint8_t *data, uint32_t *data_len)
{
    int n;

    /* receive */
    client_addr_len = sizeof(client_addr);
    memset(&client_addr, 0, sizeof(client_addr));

    n = recvfrom(*(int *)sock, (char *)buffer, sizeof(buffer), 0,
             (struct sockaddr *)&client_addr, &client_addr_len);
    if (n < 0) {
        P_ERR("recv error");
    }

    rxPDU = new CoapPDU((uint8_t *)buffer, n);

    if (rxPDU->validate()) {
        rxPDU->printHuman();
    }

    // PRINT_ARRAY("CoAP message", rxPDU->getPayloadPointer(),
    //      rxPDU->getPayloadLength());

    uint32_t payload_len = rxPDU->getPayloadLength();
    if (*data_len >= payload_len) {
        memcpy(data, rxPDU->getPayloadPointer(), payload_len);
        *data_len = payload_len;
    } else {
        P_ERR("insufficient space in buffer");
    }

    txPDU->reset();
    txPDU->setVersion(rxPDU->getVersion());
    txPDU->setMessageID(rxPDU->getMessageID());
    txPDU->setToken(rxPDU->getTokenPointer(), rxPDU->getTokenLength());

    if (rxPDU->getType() == CoapPDU::COAP_CONFIRMABLE) {
        txPDU->setType(CoapPDU::COAP_ACKNOWLEDGEMENT);
    } else {
        txPDU->setType(CoapPDU::COAP_NON_CONFIRMABLE);
    }

    delete rxPDU;
    return ok;
}

static void prepare_CoAP_response(CoapPDU *recvPDU, CoapPDU *sendPDU)
{
    uint8_t response_msg[] = { "This is a response!" };
    sendPDU->reset();
    sendPDU->setVersion(1);
    sendPDU->setType(CoapPDU::COAP_ACKNOWLEDGEMENT);
    sendPDU->setCode(CoapPDU::COAP_CONTENT);
    sendPDU->setToken(recvPDU->getTokenPointer(),
              recvPDU->getTokenLength());
    sendPDU->setMessageID(recvPDU->getMessageID());
    sendPDU->setPayload(response_msg, sizeof(response_msg));

    P_INFO("=============================================================");
    P_INFO("Unprotected response:");
    if (sendPDU->validate()) {
        sendPDU->printHuman();
    }
}

int
main(int argc, char **argv) {
    
    // Get options
    int c;
    strcpy(cfg_file, "/etc/edhoc_oscore.cfg");
    while ( ( c = getopt ( argc, argv, "c:v:h" ) ) != -1 ) {
        switch ( c ) {
            case 'v':
                if (strcmp(optarg,"0") != 0 && strcmp(optarg,"1") != 0 && strcmp(optarg,"2") != 0) {
                    printf("verbose not valid: %s",optarg);
                    exit(EXIT_FAILURE);
                } else if (strcmp(optarg,"0") == 0) {
                    set_verb_level(CI_VERB_ERROR_);
                } else if (strcmp(optarg,"2") == 0) {
                    set_verb_level(CI_VERB_DEBUG_);
                }
                break;
            case 'c': {
                printf("configuration file: %s\n", optarg);
                strcpy(cfg_file, optarg);
                break;
            }
            case 'h': {
                fprintf(stderr, "Usage: %s [-c conf file (default /etc/edhoc_oscore.cfg)] [-v verbose_level]", argv[0]);
                return 0;
            }
            default: {
                fprintf(stderr, "Usage: %s [-c conf file (default /etc/edhoc_oscore.cfg)] [-v verbose_level]", argv[0]);
                exit(EXIT_FAILURE);
            }
        }
    }
    
    
    int sockfd;
    uint8_t prk_exporter[32];
    uint8_t oscore_master_secret[16];
    uint8_t oscore_master_salt[8];

    /* edhoc declarations */
    uint8_t PRK_out[PRK_DEFAULT_SIZE];
    uint8_t err_msg[ERR_MSG_DEFAULT_SIZE];
    uint32_t err_msg_len = sizeof(err_msg);
    uint8_t ad_1[AD_DEFAULT_SIZE];
    uint32_t ad_1_len = sizeof(ad_1);
    uint8_t ad_3[AD_DEFAULT_SIZE];
    uint32_t ad_3_len = sizeof(ad_1);
    
    uint16_t cred_num = 1;
    struct other_party_cred cred_i;
    struct edhoc_responder_context c_r;
        
    /* ******************************** */
    /* BEGIN Reading configuration file */
    /* ******************************** */
    // move to a function
    
    //auth_entries
    const char *auth_entry_name = NULL;
    int id_cred_x_l = 0;
    int method = 0;
    int cred_x_l = 0;
    int private_key_l = 0;    
    //connections
    const char *conn_name = NULL;
    int local_autostartup = 0;
    const char *local_auth_cred_ref = NULL;
    int c_x_l = 0;
    int suites_x_l = 0;
    int ead_a_l = 0;
    int ead_b_l = 0;
    int remote_id_cred_x_l = 0;
    const char *remote_auth_method = NULL;
    int remote_cred_x_l = 0;
    bool key_confirmation = false;
    int set_oscore = 1;
    
    //libconfig 
    config_t cfg, *cf;
    const config_setting_t *connections;
    const config_setting_t *connection;
    const config_setting_t *auth_entries;
    const config_setting_t *auth_entry;
    const config_setting_t *conn_local;
    const config_setting_t *conn_remote;
    int count = 0; 
    int i =0;

    cf = &cfg;
    config_init(cf);

    if (!config_read_file(cf, cfg_file)) {
        P_ERR("Can't open %s %s:%d - %s",
            cfg_file,
            config_error_file(cf),
            config_error_line(cf),
            config_error_text(cf));
        config_destroy(cf);
        return(EXIT_FAILURE);
    }
    
    // read auth_entry section 
    auth_entries = config_lookup(cf, "auth-entry");
    if (auth_entries != NULL){
        count = config_setting_length(auth_entries);
        P_DBG("I have %i auth_entries:", count);
        for (i = 0; i < count; i++) {
            auth_entry = config_setting_get_elem(auth_entries, i);          
            config_setting_lookup_string(auth_entry,"name",&auth_entry_name);
            P_DBG("auth-entry name: %s", auth_entry_name);  
            
			const char *id_cred_x_b64 = NULL;
            if (config_setting_lookup_string(auth_entry, "id-cred-x", &id_cred_x_b64)) {
                P_DBG("auth-entry id-cred-x base64: %s", id_cred_x_b64);
				size_t out_len;
				id_cred_x = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
				if (!mbedtls_base64_decode(id_cred_x,MAX_LEN,&out_len,(unsigned char *)id_cred_x_b64, strlen(id_cred_x_b64))) {
					id_cred_x_l  = (int)out_len;
					P_DBG("id-cred-x len: %i",id_cred_x_l);
				} else P_ERR("Error decoding id-cred-x ");	
            } else 
                P_DBG("auth-entry id-cred-x not defined");
            
            const char *auth_method = NULL;
            if (config_setting_lookup_string(auth_entry, "auth-method", &auth_method)) {
                P_DBG("auth-entry auth-method: %s", auth_method);
                if (!strcmp(auth_method,"static-dh-key")) method = 3;
            } else 
                P_DBG("auth-entry auth_method not defined, using default");
        
			const char *cred_x_b64 = NULL;
            if (config_setting_lookup_string(auth_entry, "cred-x", &cred_x_b64)) {
                P_DBG("auth-entry cred-x base64: %s", cred_x_b64);
				size_t out_len;
				cred_x = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
				if (!mbedtls_base64_decode(cred_x,MAX_LEN,&out_len,(unsigned char *)cred_x_b64, strlen(cred_x_b64))) {
					cred_x_l  = (int)out_len;
					P_DBG("cred-x len: %i",cred_x_l);
				} else P_ERR("Error decoding cred-x ");	
            } else 
                P_DBG("auth-entry cred-x not defined");
			          
			const char *private_key_b64 = NULL;
            if (config_setting_lookup_string(auth_entry, "private-key", &private_key_b64)) {
                P_DBG("auth-entry private-key base64: %s", private_key_b64);
				size_t out_len;
				private_key = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
				if (!mbedtls_base64_decode(private_key,MAX_LEN,&out_len,(unsigned char *)private_key_b64, strlen(private_key_b64))) {
					private_key_l  = (int)out_len;
					P_DBG("private-key len: %i",private_key_l);
				} else P_ERR("Error decoding private-key ");	
            } else 
                P_DBG("auth-entry private-key not defined");    
        }
    }
    
    // read connection section
    connections = config_lookup(cf, "connection");
    if (connections != NULL){
        count = config_setting_length(connections);
        P_DBG("I have %i connections:", count);
        for (i = 0; i < count; i++) {
            connection = config_setting_get_elem(connections, i);       
            config_setting_lookup_string(connection,"name",&conn_name);
            P_DBG("connectio _name: %s", conn_name);    
            int key_c = 0;
            if (config_setting_lookup_bool(connection, "key-confirmation", &key_c)) {
                P_DBG("connection key-confirmation: %i", key_c);
                if (key_c == 1) key_confirmation = true;
            } else 
                P_DBG("connection key-confirmation not defined");
            
            if (config_setting_lookup_bool(connection, "set-oscore", &set_oscore)) {
                P_DBG("connection set-oscore: %i", set_oscore);
            } else 
                P_DBG("connection set-oscore not defined");
            
            conn_local = config_setting_get_member (connection,"local");
            if (conn_local != NULL) {           
                if (config_setting_lookup_bool(conn_local, "autostartup", &local_autostartup)) {
                    P_DBG("local autostartup: %i", local_autostartup);
                }
                else
                    P_DBG("local autostartup not defined, using default");
                if (config_setting_lookup_string(conn_local, "auth-cred-ref", &local_auth_cred_ref)) {
                    P_DBG("local auth-cred-ref: %s", local_auth_cred_ref);
                } else 
                    P_DBG("local id-cred-x-ref not defined");   
				
				
				const char *local_suites_x_b64 = NULL;
			    if (config_setting_lookup_string(conn_local, "suites-x", &local_suites_x_b64)) {
                    P_DBG("local suites-x base54: %s", local_suites_x_b64);
					size_t out_len;
					suites_x = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
					if (!mbedtls_base64_decode(suites_x,MAX_LEN,&out_len,(unsigned char *)local_suites_x_b64, strlen(local_suites_x_b64))) {
						suites_x_l  = (int)out_len;
						P_DBG("local-suites-x len: %i",suites_x_l);
					} else P_ERR("Error decoding suites-x");	
                } else 
                    P_DBG("local suites_x not defined");   
				 
				
				const char *c_x_b64 = NULL; 
                if (config_setting_lookup_string(conn_local, "c-x", &c_x_b64)) {
                    P_DBG("c-x base64: %s", c_x_b64);
					size_t out_len;
					c_x = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
					if (!mbedtls_base64_decode(c_x,MAX_LEN,&out_len,(unsigned char *)c_x_b64, strlen(c_x_b64))) {
						c_x_l  = (int)out_len;
						P_DBG("c-x len: %i",c_x_l);
					} else P_ERR("Error decoding c-x ");	
                } else 
                    P_DBG("c-x not defined");  
			
				const char *ead_a_b64 = NULL;  
                if (config_setting_lookup_string(conn_local, "ead-a", &ead_a_b64)) {
                    P_DBG("ead-a base64: %s", ead_a_b64);
					size_t out_len;
					ead_a = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
					if (!mbedtls_base64_decode(ead_a,MAX_LEN,&out_len,(unsigned char *)ead_a_b64, strlen(ead_a_b64))) {
						ead_a_l  = (int)out_len;
						P_DBG("ead-a len: %i",ead_a_l);
					} else P_ERR("Error decoding ead-a");	
                } else 
                    P_DBG("local ead-a not defined"); 
				
				const char *ead_b_b64 = NULL;  
                if (config_setting_lookup_string(conn_local, "ead-b", &ead_b_b64)) {
                    P_DBG("ead-b base64: %s", ead_b_b64);
					size_t out_len;
					ead_b = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
					if (!mbedtls_base64_decode(ead_b,MAX_LEN,&out_len,(unsigned char *)ead_b_b64, strlen(ead_b_b64))) {
						ead_b_l  = (int)out_len;
						P_DBG("ead-b len: %i",ead_b_l);
					} else P_ERR("Error decoding ead-b");	
                } else 
                    P_DBG("local ead-b not defined");                   
            }
            conn_remote = config_setting_get_member (connection,"remote");
            if (conn_remote != NULL) { 
            
				const char *remote_id_cred_x_b64 = NULL;
				if (config_setting_lookup_string(conn_remote, "id-cred-x", &remote_id_cred_x_b64)) {
                    P_DBG("remote id_cred-x base64: %s", remote_id_cred_x_b64);
					size_t out_len;
					remote_id_cred_x = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
					if (!mbedtls_base64_decode(remote_id_cred_x,MAX_LEN,&out_len,(unsigned char *)remote_id_cred_x_b64, strlen(remote_id_cred_x_b64))) {
						remote_id_cred_x_l  = (int)out_len;
						P_DBG("remote id-cred-x len: %i",remote_id_cred_x_l);
					} else P_ERR("Error decoding remote id-cred-x ");	
                } else 
                    P_DBG("remote id-cred-x not defined");  
				
				
                if (config_setting_lookup_string(conn_remote, "auth-method", &remote_auth_method)) {
                    P_DBG("remote auth-method: %s", remote_auth_method);
                } else 
                    P_DBG("remote auth-method not defined"); 
				
				const char *remote_cred_x_b64 = NULL;   
                if (config_setting_lookup_string(conn_remote, "cred-x", &remote_cred_x_b64)) {
                    P_DBG("remote cred-x base64: %s", remote_cred_x_b64);
					size_t out_len;
					remote_cred_x = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
					if (!mbedtls_base64_decode(remote_cred_x,MAX_LEN,&out_len,(unsigned char *)remote_cred_x_b64, strlen(remote_cred_x_b64))) {
						remote_cred_x_l  = (int)out_len;
						P_DBG("remote-cred-x len: %i",remote_cred_x_l);
					} else P_ERR("Error decoding remote-cred-x ");	
                } else 
                    P_DBG("remote cred-x not defined");                 
            }   
        }       
    }
    // read local-resource list
    const config_setting_t *local_resources;
    const config_setting_t *local_resource;
    const char *local_uri = NULL;
    const char *local_policy = NULL;
    const char *local_ref = NULL;
    local_resources = config_lookup(cf, "local-resource");
    if (local_resources != NULL){
        // num of contexts 
        count = config_setting_length(local_resources);
        P_DBG("I have %i local-resources:", count);     
        //for each local read elements 
        //WARNING: just one local is read, more local entries
        for (i = 0; i < count; i++) {
            local_resource = config_setting_get_elem(local_resources, i);
            config_setting_lookup_string(local_resource,"local",&local_uri);
            P_DBG("local-resource uri: %s", local_uri); 
            
            config_setting_lookup_string(local_resource,"policy",&local_policy);
            P_DBG("local-resource policy: %s", local_policy);
            
            config_setting_lookup_string(local_resource,"conn-ref",&local_ref);
            P_DBG("local-resource connection ref: %s", local_ref);
        }
        if (strcmp(local_ref,conn_name)) {
            ERR("conn-ref not found in contexts");
            cleanup();
            return 0;
        }
    }
    else {
        ERR("local resources not defined.");
        cleanup();
        return 0;   
    }   
    if (local_uri == NULL) {
        P_ERR("local resource URI not defined");
        cleanup();
        return 0;
    }
    
    coap_uri_t coap_uri;  
    int result = coap_split_uri((unsigned char *)local_uri, strlen(local_uri), &coap_uri);
    if (result != 0) {
        P_ERR("URI parse error");
        config_destroy(cf);
        cleanup();
        return 0;
    }
    char host [MAXLINE];
    strncpy(host,(char *)coap_uri.host.s,coap_uri.host.length);
    host[coap_uri.host.length] = '\0';
    P_DBG("URI host name: %s",host);
    P_DBG("URI host name len: %i",(int)strlen(host));
    P_DBG("URI port: %i",coap_uri.port); 
    P_DBG("URI path: %s",coap_uri.path.s);
    if (coap_uri.path.s == NULL) {
        P_INFO("URI path not defined or incorrectly defined. Ignored");
    } else 
        P_INFO("URI path defined, but ignored"); //TBD

    /* ******************************** */
    /* END Reading configuration file */
    /* ******************************** */  
    
    
    TRY_EXPECT(start_coap_server(&sockfd, host, coap_uri.port), 0);
    
    
    /* ****************************** */
    /* BEGIN Init EDHOC parameters    */
    /* ****************************** */
    c_r.c_r.len = c_x_l;
    c_r.c_r.ptr = (uint8_t *)c_x;
    // key_confirmation
    c_r.msg4 = key_confirmation;
    //c_r.method = (enum method_type)method;  // WARNING: method is not used in responder
    // suites_r
    c_r.suites_r.len = suites_x_l;
    c_r.suites_r.ptr = (uint8_t *)suites_x;
    // ead_2
    c_r.ead_2.len = ead_a_l;
    c_r.ead_2.ptr = (uint8_t *)ead_a;
    //ead_4
    c_r.ead_4.len = ead_b_l;
    c_r.ead_4.ptr = (uint8_t *)ead_b;
    //id_cred_r
    c_r.id_cred_r.len = id_cred_x_l;
    c_r.id_cred_r.ptr = (uint8_t *)id_cred_x;
    // cred_r
    c_r.cred_r.len = cred_x_l;
    c_r.cred_r.ptr = (uint8_t *)cred_x;
    // from test_vector_p256_v16 vec_num = 1 g_y_raw and y_raw. they should be generated in realtime
    const uint8_t g_y[] = {0x41, 0x97, 0x01, 0xd7, 0xf0, 0x0a, 0x26, 0xc2, 0xdc, 0x58, 0x7a, 0x36, 0xdd, 0x75, 0x25, 0x49, 0xf3, 0x37, 0x63, 0xc8, 0x93, 0x42, 0x2c, 0x8e, 0xa0, 0xf9, 0x55, 0xa1, 0x3a, 0x4f, 0xf5, 0xd5};
    const uint8_t y[] = {0xe2, 0xf4, 0x12, 0x67, 0x77, 0x20, 0x5e, 0x85, 0x3b, 0x43, 0x7d, 0x6e, 0xac, 0xa1, 0xe1, 0xf7, 0x53, 0xcd, 0xcc, 0x3e, 0x2c, 0x69, 0xfa, 0x88, 0x4b, 0x0a, 0x1a, 0x64, 0x09, 0x77, 0xe4, 0x18};
    c_r.g_y.len = sizeof(g_y);
    c_r.g_y.ptr = (uint8_t *)g_y;
    c_r.y.len = sizeof(y);
    c_r.y.ptr = (uint8_t *)y;
    
    // privake_key
    if (method == INITIATOR_SK_RESPONDER_SK) {
        c_r.sk_r.len = private_key_l;
        c_r.sk_r.ptr = (uint8_t *)private_key;
    } else {
        // static DH keys
        // from test_vector_p256_v16 vec_num = 5 g_r_raw. It has to be extracted from cred_r.
        const uint8_t g_r[] = {0xbb, 0xc3, 0x49, 0x60, 0x52, 0x6e, 0xa4, 0xd3, 0x2e, 0x94, 0x0c, 0xad, 0x2a, 0x23, 0x41, 0x48, 0xdd, 0xc2, 0x17, 0x91, 0xa1, 0x2a, 0xfb, 0xcb, 0xac, 0x93, 0x62, 0x20, 0x46, 0xdd, 0x44, 0xf0};
        c_r.g_r.len = sizeof(g_r);
        c_r.g_r.ptr = (uint8_t *)g_r;
        c_r.r.len = private_key_l;
        c_r.r.ptr = (uint8_t *)private_key;   
    }
    
    // id_cred_i
    cred_i.id_cred.len = remote_id_cred_x_l;
    cred_i.id_cred.ptr = (uint8_t *)remote_id_cred_x;
    //cred_i
    cred_i.cred.len = remote_cred_x_l;
    cred_i.cred.ptr = remote_cred_x;
    
    if (method == INITIATOR_SK_RESPONDER_SK) {
        // from test_vector_p256_v16 vec_num = 1 ca_r and ca_pk. ca_r is not in cred_r!!
        const uint8_t ca_i[] = {0x30, 0x82, 0x01, 0x18, 0x30, 0x81, 0xc0, 0xa0, 0x03, 0x02, 0x01, 0x02, 0x02, 0x04, 0x61, 0xe9, 0x97, 0xc5, 0x30, 0x0a, 0x06, 0x08, 0x2a, 0x86, 0x48, 0xce, 0x3d, 0x04, 0x03, 0x02, 0x30, 0x15, 0x31, 0x13, 0x30, 0x11, 0x06, 0x03, 0x55, 0x04, 0x03, 0x0c, 0x0a, 0x45, 0x44, 0x48, 0x4f, 0x43, 0x20, 0x52, 0x6f, 0x6f, 0x74, 0x30, 0x1e, 0x17, 0x0d, 0x32, 0x32, 0x30, 0x31, 0x32, 0x30, 0x31, 0x37, 0x31, 0x31, 0x33, 0x33, 0x5a, 0x17, 0x0d, 0x32, 0x39, 0x31, 0x32, 0x33, 0x31, 0x32, 0x33, 0x30, 0x30, 0x30, 0x30, 0x5a, 0x30, 0x15, 0x31, 0x13, 0x30, 0x11, 0x06, 0x03, 0x55, 0x04, 0x03, 0x0c, 0x0a, 0x45, 0x44, 0x48, 0x4f, 0x43, 0x20, 0x52, 0x6f, 0x6f, 0x74, 0x30, 0x59, 0x30, 0x13, 0x06, 0x07, 0x2a, 0x86, 0x48, 0xce, 0x3d, 0x02, 0x01, 0x06, 0x08, 0x2a, 0x86, 0x48, 0xce, 0x3d, 0x03, 0x01, 0x07, 0x03, 0x42, 0x00, 0x04, 0x27, 0xec, 0xf4, 0xb4, 0x66, 0xd3, 0xcd, 0x61, 0x14, 0x4c, 0x94, 0x40, 0x21, 0x83, 0x8d, 0x57, 0xbf, 0x67, 0x01, 0x97, 0x33, 0x78, 0xa1, 0x5b, 0x3f, 0x5d, 0x27, 0x57, 0x5d, 0x34, 0xc4, 0xa9, 0x7b, 0x79, 0xe0, 0xf2, 0x4b, 0x44, 0x6b, 0xca, 0x67, 0xe1, 0x3d, 0x75, 0xd0, 0x95, 0x73, 0x12, 0x4b, 0x49, 0xb8, 0x38, 0xb1, 0x09, 0x73, 0xf0, 0xfb, 0x67, 0xe1, 0x26, 0x05, 0x1c, 0x95, 0x95, 0x30, 0x0a, 0x06, 0x08, 0x2a, 0x86, 0x48, 0xce, 0x3d, 0x04, 0x03, 0x02, 0x03, 0x47, 0x00, 0x30, 0x44, 0x02, 0x20, 0x13, 0x73, 0x43, 0x26, 0xf2, 0xca, 0x35, 0xd1, 0xae, 0xdb, 0x6d, 0x5e, 0x1c, 0x8e, 0xb7, 0xb9, 0x65, 0xda, 0x67, 0xea, 0xd3, 0x31, 0x4e, 0x50, 0x29, 0x09, 0xb9, 0xd7, 0x57, 0xcb, 0xa1, 0x68, 0x02, 0x20, 0x49, 0xba, 0x0b, 0xa4, 0xf0, 0x6e, 0xfe, 0x8c, 0x0d, 0x9c, 0x3d, 0x31, 0x15, 0xeb, 0x9c, 0x96, 0xca, 0x46, 0xd1, 0x28, 0x49, 0x9b, 0x68, 0x95, 0x7d, 0x0a, 0x85, 0xaf, 0x13, 0x6b, 0xf3, 0x06};
        //At least ca_pk should be extracted from ca
        const uint8_t ca_i_pk[] = {0x04, 0x27, 0xec, 0xf4, 0xb4, 0x66, 0xd3, 0xcd, 0x61, 0x14, 0x4c, 0x94, 0x40, 0x21, 0x83, 0x8d, 0x57, 0xbf, 0x67, 0x01, 0x97, 0x33, 0x78, 0xa1, 0x5b, 0x3f, 0x5d, 0x27, 0x57, 0x5d, 0x34, 0xc4, 0xa9, 0x7b, 0x79, 0xe0, 0xf2, 0x4b, 0x44, 0x6b, 0xca, 0x67, 0xe1, 0x3d, 0x75, 0xd0, 0x95, 0x73, 0x12, 0x4b, 0x49, 0xb8, 0x38, 0xb1, 0x09, 0x73, 0xf0, 0xfb, 0x67, 0xe1, 0x26, 0x05, 0x1c, 0x95, 0x95};
        cred_i.ca.len = sizeof(ca_i);
        cred_i.ca.ptr = (uint8_t *)ca_i;
        cred_i.ca_pk.len = sizeof(ca_i_pk);
        cred_i.ca_pk.ptr = (uint8_t *)ca_i_pk;
    } else {
        // from test_vector_p256_v16 vec_num = 5 g_i_raw. It has to be extracted from cred_i.cred.ptr
        const uint8_t g_i[] = {0xac, 0x75, 0xe9, 0xec, 0xe3, 0xe5, 0x0b, 0xfc, 0x8e, 0xd6, 0x03, 0x99, 0x88, 0x95, 0x22, 0x40, 0x5c, 0x47, 0xbf, 0x16, 0xdf, 0x96, 0x66, 0x0a, 0x41, 0x29, 0x8c, 0xb4, 0x30, 0x7f, 0x7e, 0xb6};
        cred_i.g.len = sizeof(g_i);
        cred_i.g.ptr = (uint8_t *)g_i;
    }
    
    c_r.sock = &sockfd;
    
    /* ****************************** */
    /* END Init EDHOC parameters    */
    /* ****************************** */
    // move to a function
    
    
    /* *********************************************************** */
    /* BEGIN EDHOC negotiation and OSCORE context derivation       */
    /* *********************************************************** */   

    TRY(edhoc_responder_run(&c_r, &cred_i, cred_num, err_msg, &err_msg_len,
                (uint8_t *)&ad_1, &ad_1_len, (uint8_t *)&ad_3,
                &ad_3_len, PRK_out, sizeof(PRK_out),
                tx, rx));
    PRINT_ARRAY("PRK_out", PRK_out, sizeof(PRK_out));

    TRY(prk_out2exporter(SHA_256, PRK_out, sizeof(PRK_out), prk_exporter));
    PRINT_ARRAY("prk_exporter", prk_exporter, sizeof(prk_exporter));

    TRY(edhoc_exporter(SHA_256, OSCORE_MASTER_SECRET, prk_exporter,
               sizeof(prk_exporter), oscore_master_secret,
               sizeof(oscore_master_secret)));
    PRINT_ARRAY("OSCORE Master Secret", oscore_master_secret,
            sizeof(oscore_master_secret));

    TRY(edhoc_exporter(SHA_256, OSCORE_MASTER_SALT, prk_exporter,
               sizeof(prk_exporter), oscore_master_salt,
               sizeof(oscore_master_salt)));
    PRINT_ARRAY("OSCORE Master Salt", oscore_master_salt,
            sizeof(oscore_master_salt));

    
    /*OSCORE contex initialization*/
    struct context c_server;
    oscore_init_params params = {
        SERVER,
        sizeof(oscore_master_secret),
        oscore_master_secret,
        T1__RECIPIENT_ID_LEN,
        (uint8_t *)T1__RECIPIENT_ID,
        T1__SENDER_ID_LEN,
        (uint8_t *)T1__SENDER_ID,
        T1__ID_CONTEXT_LEN,
        (uint8_t *)T1__ID_CONTEXT,
        sizeof(oscore_master_salt),
        oscore_master_salt,
        OSCORE_AES_CCM_16_64_128,
        OSCORE_SHA_256,
    };
    TRY(oscore_context_init(&params, &c_server));   
    
    
    /* *********************************************************** */
    /* END EDHOC negotiation and OSCORE context derivation       */
    /* *********************************************************** */   



    /* *********************************************************** */
    /* BEGIN CoAP+OSCORE exchange                                  */
    /* *********************************************************** */   

    int err, n;
    char buffer[MAXLINE];
    CoapPDU *recvPDU, *sendPDU = new CoapPDU();
    uint8_t coap_rx_buf[256];
    uint32_t coap_rx_buf_len = 0;
    uint8_t buf_oscore[256];
    uint32_t buf_oscore_len = sizeof(buf_oscore);
    bool oscore_flag;

    while (1) {
        client_addr_len = sizeof(client_addr);
        memset(&client_addr, 0, sizeof(client_addr));

        n = recvfrom(sockfd, (char *)buffer, sizeof(buffer), 0,
                 (struct sockaddr *)&client_addr, &client_addr_len);
        if (n < 0) {
            return n;
        }

        TRY(oscore2coap((uint8_t *)buffer, n, coap_rx_buf,
                &coap_rx_buf_len, &oscore_flag, &c_server));

        if (oscore_flag) {
            /*we received an OSOCRE packet*/
            recvPDU = new CoapPDU((uint8_t *)coap_rx_buf,
                          coap_rx_buf_len);

            P_INFO("=====================================================");
            P_INFO("OSCORE packet received and converted to CoAP:");
            if (recvPDU->validate()) {
                recvPDU->printHuman();
            }

            prepare_CoAP_response(recvPDU, sendPDU);

            TRY(coap2oscore(sendPDU->getPDUPointer(),
                    sendPDU->getPDULength(), buf_oscore,
                    &buf_oscore_len, &c_server));

            err = sendto(sockfd, buf_oscore, buf_oscore_len, 0,
                     (struct sockaddr *)&client_addr,
                     client_addr_len);
            if (err < 0)
                return err;

        } else {
            /*we received a CoAP packet*/
            recvPDU = new CoapPDU((uint8_t *)buffer, n);
            P_INFO("=====================================================");
            P_INFO("Unprotected CoAP packet received:");
            if (recvPDU->validate()) {
                recvPDU->printHuman();
            }

            prepare_CoAP_response(recvPDU, sendPDU);

            err = sendto(sockfd, sendPDU->getPDUPointer(),
                     sendPDU->getPDULength(), 0,
                     (struct sockaddr *)&client_addr,
                     client_addr_len);
            if (err < 0)
                return err;
        }
    }
    
    /* *********************************************************** */
    /* BEGIN CoAP+OSCORE exchange                                  */
    /* *********************************************************** */   
    cleanup();
    return 0;
}
