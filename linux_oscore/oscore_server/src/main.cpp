/*
   Copyright (c) 2021 Fraunhofer AISEC. See the COPYRIGHT
   file at the top-level directory of this distribution.

   Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
   http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
   <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
   option. This file may not be copied, modified, or distributed
   except according to those terms.
*/


/* Warning:
- Configuration file is expected at /etc/oscore.cfg
- Target and local resource list are not implemented yet, so IP and service URI are hardcoded
- From the contexts list only the first element of the list is read
*/

#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <netdb.h>

#include <libconfig.h>


extern "C" {
#include "oscore.h"
#include "sock.h"
#include "inc/log.h"
#include "inc/utils.h"
}
#include "cantcoap.h"
#include "coap.h"
#include "oscore_test_vectors.h"


#define USE_IPV4
int port = 5683;


unsigned char *m_secret = NULL;
unsigned char *m_salt = NULL;
unsigned char *s_id = NULL;
unsigned char *r_id = NULL;
unsigned char *c_id = NULL;
char cfg_file [MAXLINE];

void cleanup() {
	free(m_secret);
	free(m_salt);
	free(c_id);
	free(r_id);
	free(s_id);
			
}

static void prepare_CoAP_response(CoapPDU *recvPDU, CoapPDU *sendPDU)
{
	uint8_t response_msg[] = { "This is a response!" };
	sendPDU->reset();
	sendPDU->setVersion(1);
	sendPDU->setType(CoapPDU::COAP_ACKNOWLEDGEMENT);
	sendPDU->setCode(CoapPDU::COAP_CONTENT);
	sendPDU->setToken(recvPDU->getTokenPointer(),
			  recvPDU->getTokenLength());
	sendPDU->setMessageID(recvPDU->getMessageID());
	sendPDU->setPayload(response_msg, sizeof(response_msg));

	P_INFO("=============================================================");
	P_INFO("Unprotected response:");
	if (sendPDU->validate()) {
		sendPDU->printHuman();
	}
}


int 
main(int argc, char **argv)
{
	
    // Get options
	strcpy(cfg_file, "/etc/oscore.cfg");
	
    int c;
    while ( ( c = getopt ( argc, argv, "c:v:h" ) ) != -1 ) {
        switch ( c ) {
            case 'v':
                if (strcmp(optarg,"0") != 0 && strcmp(optarg,"1") != 0 && strcmp(optarg,"2") != 0) {
                    printf("verbose not valid: %s",optarg);
                    exit(EXIT_FAILURE);
                } else if (strcmp(optarg,"0") == 0) {
                    set_verb_level(CI_VERB_ERROR_);
                } else if (strcmp(optarg,"2") == 0) {
                    set_verb_level(CI_VERB_DEBUG_);
                }
                break;
			case 'c': {
				printf("configuration file: %s\n", optarg);
				strcpy(cfg_file, optarg);
				break;
			}
            case 'h': {
                fprintf(stderr, "Usage: %s [-c config_file] [-v verbose_level]\n", argv[0]);
                return 0;
            }
            default: {
                fprintf(stderr, "Usage: %s [-c config_file] [-v verbose_level]\n", argv[0]);
                exit(EXIT_FAILURE);
            }
        }
    }
	
	setbuf(stdout, NULL); //disable printf buffereing
	err r;
	int err, n;
	char buffer[MAXLINE];
	socklen_t client_addr_len;
	struct context c_server;
	CoapPDU *recvPDU, *sendPDU = new CoapPDU();
	uint8_t coap_rx_buf[256];
	uint32_t coap_rx_buf_len = 0;
	uint8_t buf_oscore[256];
	uint32_t buf_oscore_len = sizeof(buf_oscore);
	bool oscore_flag;
	int sockfd;

	// variables to read a context entry
	config_t cfg, *cf;
	const char *context_name = NULL;
	uint8_t master_secret_l = 0;
	uint8_t master_salt_l = 0;
	int aead_alg, hkdf_alg;
    AEAD_algorithm aead_alg_v;
	hkdf hkdf_alg_v;
	uint8_t recipient_id_l = 0;
	uint8_t sender_id_l = 0;
	uint8_t context_id_l = 0;
	const config_setting_t *contexts;
	const config_setting_t *context;
	const config_setting_t *common_ctx_s;
	const config_setting_t *recipient_ctx_s;
	const config_setting_t *sender_ctx_s;
	
	int count = 0; 
	int i =0;

	cf = &cfg;
	config_init(cf);

	if (!config_read_file(cf, cfg_file)) {
		P_ERR("Can't open %s %s:%d - %s",
			cfg_file,
			config_error_file(cf),
			config_error_line(cf),
			config_error_text(cf));
		config_destroy(cf);
		return(EXIT_FAILURE);
	}
	// lookup for contexts list element in oscore.cfg
	contexts = config_lookup(cf, "context");
	if (contexts != NULL){
		// num of contexts 
		count = config_setting_length(contexts);
		P_DBG("I have %i contexts:", count);
		
		//for each context read elements 
		//WARNING: just one context is read, more contexts entries TBD
		for (i = 0; i < count; i++) {
			
			context = config_setting_get_elem(contexts, i);
			config_setting_lookup_string(context,"name",&context_name);
			P_DBG("context-name: %s", context_name);	
			
			common_ctx_s = config_setting_get_member (context,"common-ctx");
			if (common_ctx_s != NULL) {
				
				// read master secret
				const char *master_secret_b64 = NULL;
				if (config_setting_lookup_string(common_ctx_s, "master-key", &master_secret_b64)) {	
					P_DBG("master secret base64 %s",master_secret_b64);
					size_t out_len;
					m_secret = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
					if (!mbedtls_base64_decode(m_secret,MAX_LEN,&out_len,(unsigned char *)master_secret_b64, strlen(master_secret_b64))) {
						master_secret_l  = (int)out_len;
						P_DBG("master-secret len: %i",master_secret_l);
					} else P_ERR("Error decoding master-secret");
				} else 
					P_DBG("master-secret not defined");
			
			    const char *master_salt_b64 = NULL;
				if (config_setting_lookup_string(common_ctx_s, "master-salt", &master_salt_b64)) {
					P_DBG("master salt base64 %s",master_salt_b64);
					size_t out_len;
					m_salt = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
					if (!mbedtls_base64_decode(m_salt,MAX_LEN,&out_len,(unsigned char *)master_salt_b64, strlen(master_salt_b64))) {
						master_salt_l  = (int)out_len;
						P_DBG("master-salt len: %i",master_salt_l);
					} else P_ERR("Error decoding master-salt");
				}
				else {
					P_DBG("master_salt not defined");
				}
			
				if (config_setting_lookup_int(common_ctx_s, "aead-alg", &aead_alg)) {
					P_DBG("aead alg: %i", aead_alg);
					if (aead_alg == 10)
						aead_alg_v = OSCORE_AES_CCM_16_64_128;
				}
				else {
					P_DBG("aead algorithm not defined, using default");
					aead_alg_v = OSCORE_AES_CCM_16_64_128;
				}
					
				if (config_setting_lookup_int(common_ctx_s, "hkdf-alg", &hkdf_alg)) {
					P_DBG("hkdf alg: %i", hkdf_alg);
					if (hkdf_alg == 1)
						hkdf_alg_v = OSCORE_SHA_256;
				}
				else {
					P_DBG("hkdf algorithm not defined, using default");
					hkdf_alg_v = OSCORE_SHA_256;
				}
				
				const char *context_id_b64 = NULL;
				if (config_setting_lookup_string(common_ctx_s, "id", &context_id_b64)) {
					P_DBG("context_id base64: %s",context_id_b64);
					size_t out_len;
					c_id = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
					if (!mbedtls_base64_decode(c_id,MAX_LEN,&out_len,(unsigned char *)context_id_b64, strlen(context_id_b64))) {
						context_id_l = (int)out_len;
						P_DBG("context_id len: %i",context_id_l);
					} else P_ERR("Error decoding context_id");
				} 
				else {
					P_DBG("context_id not defined");
				}
			}
			
			recipient_ctx_s = config_setting_get_member (context,"recipient-ctx");
			if (recipient_ctx_s != NULL) { 
				const char *recipient_id_b64 = NULL;
				if (!config_setting_lookup_string(recipient_ctx_s, "id", &recipient_id_b64)) {
					recipient_id_b64 = "01";
					P_DBG("recipient-id not defined, default used: %s", recipient_id_b64);
				}
				size_t out_len;
				r_id = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
				if (!mbedtls_base64_decode(r_id,MAX_LEN,&out_len,(unsigned char *)recipient_id_b64, strlen(recipient_id_b64))) {
					recipient_id_l = (int)out_len;
					P_DBG("recipient_id len: %i",recipient_id_l);
				} else P_ERR("Error decoding recipient-id");
			} else {
				ERR("recipient-ctx not defined.");
				cleanup();
				return 0;
			}
			
			sender_ctx_s = config_setting_get_member (context,"sender-ctx");
			if (sender_ctx_s != NULL) { 	
				const char *sender_id_b64 = NULL;		
				if (!config_setting_lookup_string(sender_ctx_s, "id", &sender_id_b64)) {
					sender_id_b64 = "00";
					P_DBG("sender-id not defined, default used: %s", sender_id_b64);
				}
				size_t out_len;
				s_id = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
				if (!mbedtls_base64_decode(s_id,MAX_LEN,&out_len,(unsigned char *)sender_id_b64, strlen(sender_id_b64))) {
					sender_id_l = (int)out_len;
					P_DBG("sender_id len: %i",sender_id_l);
				} else P_ERR("Error decoding sender-id");
			} else {
				ERR("sender-ctx not defined.");
				cleanup();
				return 0;
			}
		}
	} else {
		ERR("context not defined.");
		cleanup();
		return 0;
		
	}
	
	// read local-resource list
	const config_setting_t *local_resources;
	const config_setting_t *local_resource;
	const char *local_uri = NULL;
	const char *local_policy = NULL;
	const char *local_ref = NULL;
	local_resources = config_lookup(cf, "local-resource");
	if (local_resources != NULL){
		// num of contexts 
		count = config_setting_length(local_resources);
		P_DBG("I have %i local-resources:", count);
		
		//for each local read elements 
		//WARNING: just one local is read, more local entries
		/* TBD:
			- define a list of local resources and keep list in memory
		*/
		for (i = 0; i < count; i++) {
			local_resource = config_setting_get_elem(local_resources, i);
			config_setting_lookup_string(local_resource,"local",&local_uri);
			P_DBG("local-resource uri: %s", local_uri);	
			
			config_setting_lookup_string(local_resource,"policy",&local_policy);
			P_DBG("local-resource policy: %s", local_policy);
			
			config_setting_lookup_string(local_resource,"name-ref",&local_ref);
			P_DBG("local-resource context ref: %s", local_ref);
		}
		if (strcmp(local_ref,context_name)) {
			ERR("name-ref not found in contexts");
			cleanup();
			return 0;
		}
	}
	else {
		ERR("local resource not defined.");
		cleanup();
		return 0;
		
	}
	
	/*TBD:
		- Check if name-ref points to a valid context in the contexts list
		- Check local resource policy
			if policy = "protect" apply oscore
			if policy = "bypass" do not apply oscore, plain coap
			if policy = "discard" ... discard the request
	*/

	if (local_uri == NULL) {
		P_ERR("local resource URI not defined");
		cleanup();
		return 0;
	}
	
	coap_uri_t coap_uri;  
	int result = coap_split_uri((unsigned char *)local_uri, strlen(local_uri), &coap_uri);
	if (result != 0) {
		P_ERR("URI parse error");
		config_destroy(cf);
		cleanup();
		return 0;
	}
	char host [100];
	strncpy(host,(char *)coap_uri.host.s,coap_uri.host.length);
	host[coap_uri.host.length] = '\0';
	P_DBG("URI host name: %s",host);
	P_DBG("URI host name len: %i",(int)strlen(host));
	P_DBG("URI port: %i",coap_uri.port); 
	P_DBG("URI path: %s",coap_uri.path.s);
	if (coap_uri.path.s == NULL) {
		P_INFO("URI path not defined or incorrectly defined. Ignored");
	} else 
		P_INFO("URI path defined, but ignored"); //TBD
	
#ifdef USE_IPV4
	struct sockaddr_in servaddr;
	struct sockaddr_in client_addr;
	client_addr_len = sizeof(client_addr);
	memset(&client_addr, 0, sizeof(client_addr));
	if (strlen(host)>0) {
		struct hostent *hoste = gethostbyname(host);
		P_DBG("ip address: %s", inet_ntoa(*(struct in_addr*)hoste->h_addr));
		err = sock_init(SOCK_SERVER, inet_ntoa(*(struct in_addr*)hoste->h_addr), IPv4, &servaddr,
				sizeof(servaddr), &sockfd, coap_uri.port);
	}
	else {
		P_DBG("ip address 0.0.0.0");
		const char IPV4_SERVADDR[] = { "0.0.0.0" };
		err = sock_init(SOCK_SERVER, IPV4_SERVADDR, IPv4, &servaddr,
			sizeof(servaddr), &sockfd, coap_uri.port);
	}
	if (err < 0) {
		P_ERR("error during socket initialization (error code: %d)",err);
			config_destroy(cf);
			cleanup();
			return 0;
	}
#endif
		
	oscore_init_params params = {
			SERVER,
			master_secret_l,
			(unsigned char *)m_secret,
			sender_id_l,
			(unsigned char *)s_id,
			recipient_id_l,
			(unsigned char *)r_id,
			context_id_l,
			(unsigned char *)c_id,
			master_salt_l,
			(unsigned char *)m_salt,
			aead_alg_v,
			hkdf_alg_v,
		};
	

	r = oscore_context_init(&params, &c_server);
	if (r != ok) {
		P_ERR("Error during establishing an OSCORE security context!");
		config_destroy(cf);
		cleanup();
		return 0;
	}

	while (1) {
		n = recvfrom(sockfd, (char *)buffer, sizeof(buffer), 0,
			     (struct sockaddr *)&client_addr, &client_addr_len);
		if (n < 0)
			return n;

		r = oscore2coap((uint8_t *)buffer, n, coap_rx_buf,
				&coap_rx_buf_len, &oscore_flag, &c_server);
		if (r != ok) {
			P_ERR("Error in oscore2coap (error code %d)!", r);
		}

		if (oscore_flag) {
			/*we received an OSOCRE packet*/
			recvPDU = new CoapPDU((uint8_t *)coap_rx_buf,
					      coap_rx_buf_len);

			P_INFO("=====================================================");
			P_INFO("OSCORE packet received and converted to CoAP to resource: %s", coap_uri.path.s);
			if (recvPDU->validate()) {
				recvPDU->printHuman();
			}

			prepare_CoAP_response(recvPDU, sendPDU);

			r = coap2oscore(sendPDU->getPDUPointer(),
					sendPDU->getPDULength(), buf_oscore,
					&buf_oscore_len, &c_server);
			if (r != ok) {
				P_ERR("Error in coap2oscore (error code %d)!",
				       r);
			}

			err = sendto(sockfd, buf_oscore, buf_oscore_len, 0,
				     (struct sockaddr *)&client_addr,
				     client_addr_len);
			if (err < 0)
				return err;

		} else {
			/*we received a CoAP packet*/
			recvPDU = new CoapPDU((uint8_t *)buffer, n);
			P_INFO("=====================================================");
			P_INFO("Unprotected CoAP packet received to resource: %s", coap_uri.path.s);
			if (recvPDU->validate()) {
				recvPDU->printHuman();
			}

			prepare_CoAP_response(recvPDU, sendPDU);

			err = sendto(sockfd, sendPDU->getPDUPointer(),
				     sendPDU->getPDULength(), 0,
				     (struct sockaddr *)&client_addr,
				     client_addr_len);
			if (err < 0)
				return err;
		}
	}
	
	config_destroy(cf);
	cleanup();

	return 0;
}
