/*
   Copyright (c) 2021 Fraunhofer AISEC. See the COPYRIGHT
   file at the top-level directory of this distribution.

   Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
   http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
   <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
   option. This file may not be copied, modified, or distributed
   except according to those terms.
*/



/* Warning:
- Configuration file is expected at /etc/oscore.cfg
- Target and local resource list implemented, but read only one element for test purposes
- From the contexts list only the first element of the list is read
*/

#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <netdb.h>


#include <libconfig.h>


extern "C" {
#include "oscore.h"
#include "sock.h"
#include "inc/log.h"
#include "inc/utils.h"
}
#include "cantcoap.h"
#include "coap.h"

#include "oscore_test_vectors.h"

#define USE_IPV4


//can be modified by command line
int num_queries = 1; 
int port = 5683;
char cfg_file [MAXLINE];

struct context c_client;

unsigned char *m_secret = NULL;
unsigned char *m_salt = NULL;
unsigned char *s_id = NULL;
unsigned char *r_id = NULL;
unsigned char *c_id = NULL;

void cleanup(){
	free(m_secret);
	free(m_salt);
	free(c_id);
	free(r_id);
	free(s_id);
}



int 
main(int argc, char **argv)
{
	
  
    // Get options
    int c;
	strcpy(cfg_file, "/etc/oscore.cfg");
	
    while ( ( c = getopt ( argc, argv, "c:v:n:h" ) ) != -1 ) {
        switch ( c ) {
            case 'v':
                if (strcmp(optarg,"0") != 0 && strcmp(optarg,"1") != 0 && strcmp(optarg,"2") != 0) {
                    printf("verbose not valid: %s\n",optarg);
                    exit(EXIT_FAILURE);
                } else if (strcmp(optarg,"0") == 0) {
                    set_verb_level(CI_VERB_ERROR_);
                } else if (strcmp(optarg,"2") == 0) {
                    set_verb_level(CI_VERB_DEBUG_);
                }
                break;
	        case 'n': {
				num_queries = atoi(optarg);
				if (num_queries > 0)
					printf("num_queries: %d\n",num_queries);	
				else {
					printf("num_queries %d invalid\n",num_queries);
					return 0;
				}
				break;
	        }	
			case 'c': {
				printf("configuration file: %s\n", optarg);
				strcpy(cfg_file, optarg);
				break;
			}
            case 'h': {
                fprintf(stderr, "Usage: %s [-c conf file] [-n num_queries (default 1)]  [-v verbose_level]\n", argv[0]);
                return 0;
            }
            default: {
                fprintf(stderr, "Usage: %s [-c conf file] [-n num_queries (default 1)]  [-v verbose_level]\n", argv[0]);
                exit(EXIT_FAILURE);
            }
        }
    }
	
	

	setbuf(stdout, NULL); //disable printf buffereing
	err r;
	int err;
	char buffer[MAXLINE];
	int sockfd;



	/*construct a CoAP packet*/
	uint16_t mid1 = 256, mid2 = 0;
	uint32_t token = 0;
	int32_t n;
	uint32_t len;
	bool oscore_flag = false;
	CoapPDU *unprotected_pdu = new CoapPDU();
	CoapPDU *protected_pdu = new CoapPDU();

	// variables to read a context entry
	const char *context_name = NULL;
	uint8_t master_secret_l = 0;
	uint8_t master_salt_l = 0;
	int aead_alg, hkdf_alg;
    AEAD_algorithm aead_alg_v;
	hkdf hkdf_alg_v;
	uint8_t recipient_id_l = 0;
	uint8_t sender_id_l = 0;
	uint8_t context_id_l = 0;
	
	//libconfig elements to read oscore.cfg
	config_t cfg, *cf;
	const config_setting_t *contexts;
	const config_setting_t *context;
	const config_setting_t *common_ctx_s;
	const config_setting_t *recipient_ctx_s;
	const config_setting_t *sender_ctx_s;
	int count = 0; 
	int i =0;

	cf = &cfg;
	config_init(cf);

	if (!config_read_file(cf, cfg_file)) {
		P_ERR("Can't open %s %s:%d - %s",
			cfg_file,
			config_error_file(cf),
			config_error_line(cf),
			config_error_text(cf));
		config_destroy(cf);
		return(EXIT_FAILURE);
	}
	
	// lookup for contexts list element in oscore.cfg
	contexts = config_lookup(cf, "context");
	if (contexts != NULL){
		// num of contexts 
		count = config_setting_length(contexts);
		P_DBG("I have %i contexts:", count);
		
		//for each context read elements 
		//WARNING: just one context is read, more contexts entries TBD
		for (i = 0; i < count; i++) {
			
			context = config_setting_get_elem(contexts, i);
			config_setting_lookup_string(context,"name",&context_name);
			P_DBG("context-name: %s", context_name);	
			
			common_ctx_s = config_setting_get_member (context,"common-ctx");
			if (common_ctx_s != NULL) {
				
				// read master secret
				const char *master_secret_b64 = NULL;
				if (config_setting_lookup_string(common_ctx_s, "master-key", &master_secret_b64)) {	
					P_DBG("master secret base64 %s",master_secret_b64);
					size_t out_len;
					m_secret = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
					if (!mbedtls_base64_decode(m_secret,MAX_LEN,&out_len,(unsigned char *)master_secret_b64, strlen(master_secret_b64))) {
						master_secret_l  = (int)out_len;
						P_DBG("master-secret len: %i",master_secret_l);
					} else P_ERR("Error decoding master-secret");
				} else 
					P_DBG("master-secret not defined");
			
			    const char *master_salt_b64 = NULL;
				if (config_setting_lookup_string(common_ctx_s, "master-salt", &master_salt_b64)) {
					P_DBG("master salt base64 %s",master_salt_b64);
					size_t out_len;
					m_salt = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
					if (!mbedtls_base64_decode(m_salt,MAX_LEN,&out_len,(unsigned char *)master_salt_b64, strlen(master_salt_b64))) {
						master_salt_l  = (int)out_len;
						P_DBG("master-salt len: %i",master_salt_l);
					} else P_ERR("Error decoding master-salt");
				}
				else {
					P_DBG("master_salt not defined");
				}
			
				if (config_setting_lookup_int(common_ctx_s, "aead-alg", &aead_alg)) {
					P_DBG("aead alg: %i", aead_alg);
					if (aead_alg == 10)
						aead_alg_v = OSCORE_AES_CCM_16_64_128;
				}
				else {
					P_DBG("aead algorithm not defined, using default");
					aead_alg_v = OSCORE_AES_CCM_16_64_128;
				}
					
				if (config_setting_lookup_int(common_ctx_s, "hkdf-alg", &hkdf_alg)) {
					P_DBG("hkdf alg: %i", hkdf_alg);
					if (hkdf_alg == 1)
						hkdf_alg_v = OSCORE_SHA_256;
				}
				else {
					P_DBG("hkdf algorithm not defined, using default");
					hkdf_alg_v = OSCORE_SHA_256;
				}
				
				const char *context_id_b64 = NULL;
				if (config_setting_lookup_string(common_ctx_s, "id", &context_id_b64)) {
					P_DBG("context_id base64: %s",context_id_b64);
					size_t out_len;
					c_id = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
					if (!mbedtls_base64_decode(c_id,MAX_LEN,&out_len,(unsigned char *)context_id_b64, strlen(context_id_b64))) {
						context_id_l = (int)out_len;
						P_DBG("context_id len: %i",context_id_l);
					} else P_ERR("Error decoding context_id");
				} 
				else {
					P_DBG("context_id not defined");
				}
			}
			
			recipient_ctx_s = config_setting_get_member (context,"recipient-ctx");
			if (recipient_ctx_s != NULL) { 
				const char *recipient_id_b64 = NULL;
				if (!config_setting_lookup_string(recipient_ctx_s, "id", &recipient_id_b64)) {
					recipient_id_b64 = "01";
					P_DBG("recipient-id not defined, default used: %s", recipient_id_b64);
				}
				size_t out_len;
				r_id = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
				if (!mbedtls_base64_decode(r_id,MAX_LEN,&out_len,(unsigned char *)recipient_id_b64, strlen(recipient_id_b64))) {
					recipient_id_l = (int)out_len;
					P_DBG("recipient_id len: %i",recipient_id_l);
				} else P_ERR("Error decoding recipient-id");
			} else {
				ERR("recipient-ctx not defined.");
				cleanup();
				return 0;
			}
			
			sender_ctx_s = config_setting_get_member (context,"sender-ctx");
			if (sender_ctx_s != NULL) { 	
				const char *sender_id_b64 = NULL;		
				if (!config_setting_lookup_string(sender_ctx_s, "id", &sender_id_b64)) {
					sender_id_b64 = "00";
					P_DBG("sender-id not defined, default used: %s", sender_id_b64);
				}
				size_t out_len;
				s_id = (unsigned char*) malloc(MAX_LEN * sizeof(unsigned char));
				if (!mbedtls_base64_decode(s_id,MAX_LEN,&out_len,(unsigned char *)sender_id_b64, strlen(sender_id_b64))) {
					sender_id_l = (int)out_len;
					P_DBG("sender_id len: %i",sender_id_l);
				} else P_ERR("Error decoding sender-id");
			} else {
				ERR("sender-ctx not defined.");
				cleanup();
				return 0;
			}
		}
	} else {
		ERR("context not defined.");
		cleanup();
		return 0;
		
	}
	
	/*m_secret = (unsigned char*) malloc(master_secret_l* sizeof(unsigned char));
	convert_hex_to_unsigned_char(m_secret,master_secret_l,master_secret);
	m_salt = (unsigned char*) malloc(master_salt_l* sizeof(unsigned char));
	convert_hex_to_unsigned_char(m_salt,master_salt_l,master_salt);
	s_id = (unsigned char*) malloc(sender_id_l* sizeof(unsigned char));
	convert_hex_to_unsigned_char(s_id,sender_id_l,sender_id);
	r_id = (unsigned char*) malloc(recipient_id_l* sizeof(unsigned char));
	convert_hex_to_unsigned_char(r_id,recipient_id_l,recipient_id);
	c_id = (unsigned char*) malloc(context_id_l* sizeof(unsigned char));
	convert_hex_to_unsigned_char(c_id,context_id_l,context_id);*/
	
	// read target-resource list
	const config_setting_t *target_resources;
	const config_setting_t *target_resource;
	const char *target_uri = NULL;
	const char *target_policy = NULL;
	const char *target_ref = NULL;
	target_resources = config_lookup(cf, "target-resource");
	if (target_resources != NULL){
		// num of contexts 
		count = config_setting_length(target_resources);
		P_DBG("I have %i target-resources:", count);
		
		//for each target read elements 
		//WARNING: just one target is read, more target entries
		/* TBD:
			- define a list of target resources and keep list in memory
		*/
		for (i = 0; i < count; i++) {
			target_resource = config_setting_get_elem(target_resources, i);
			config_setting_lookup_string(target_resource,"target",&target_uri);
			P_DBG("target-resource uri: %s", target_uri);	
			
			if (target_uri == NULL) {
				P_ERR("target resource URI not defined");
				cleanup();
				return 0;
			}
			
			config_setting_lookup_string(target_resource,"policy",&target_policy);
			P_DBG("target-resource policy: %s", target_policy);
			
			config_setting_lookup_string(target_resource,"name-ref",&target_ref);
			P_DBG("target-resource context ref: %s", target_ref);
		}
	}
	else {
		ERR("target resources not defined.");
		config_destroy(cf);
		cleanup();
		return 0;
	}
	
	
	
	/*TBD:
		- Check if name-ref points to a valid context in the contexts list
		- Check target resource policy
			if policy = "protect" apply oscore
			if policy = "bypass" do not apply oscore, plain coap
			if policy = "discard" ... does it make sense a discard policy in the client?
	*/
	
	coap_uri_t coap_uri;  
	int result = coap_split_uri((unsigned char *)target_uri, strlen(target_uri), &coap_uri);
	if (result != 0) {
		P_ERR("URI parse error");
		config_destroy(cf);
		cleanup();
		return 0;
	}
	char host [100];
	strncpy(host,(char *)coap_uri.host.s,coap_uri.host.length);
	host[coap_uri.host.length] = '\0';
	P_DBG("URI host name: %s",host);
  	P_DBG("URI host name len: %i",(int)strlen(host));
	P_DBG("URI port: %i",coap_uri.port); // port can not be changed without changing sock_init definition in uoscore source code
  	P_DBG("URI path: %s",coap_uri.path.s);
	
	if (strlen(host) == 0){
		P_ERR("Host server not defined in URI");
		config_destroy(cf);
		cleanup();
		return 0;
	}
	
#ifdef USE_IPV4
	struct sockaddr_in servaddr;
	struct hostent *hoste = gethostbyname(host);
	//P_DBG("ip address: %s", inet_ntoa(*(struct in_addr*)hoste->h_addr));
	err = sock_init(SOCK_CLIENT, inet_ntoa(*(struct in_addr*)hoste->h_addr), IPv4, &servaddr,
			sizeof(servaddr), &sockfd, coap_uri.port);
	if (err < 0) {
		P_ERR("error during socket initialization (error code: %d)",err);
		config_destroy(cf);
		cleanup();
		return 0;
	}
#endif

	oscore_init_params params = {
			CLIENT,
			master_secret_l,
			(unsigned char *)m_secret,
			sender_id_l,
			(unsigned char *)s_id,
			recipient_id_l,
			(unsigned char *)r_id,
			context_id_l,
			(unsigned char *)c_id,
			master_salt_l,
			(unsigned char *)m_salt,
			aead_alg_v,
			hkdf_alg_v,
		};
	
	r = oscore_context_init(&params, &c_client);

	if (r != ok) {
		P_ERR("Error during establishing an OSCORE security context!");
		config_destroy(cf);
		cleanup();
		return 0;
	}

	uint8_t buf_oscore[256];
	uint32_t buf_oscore_len = sizeof(buf_oscore);
	uint8_t coap_rx_buf[256];
	uint32_t coap_rx_buf_len = 0;
	CoapPDU *recvPDU;
	bool request_secure_resource = true;
	uint8_t request_payload[] = { "This is some payload" };
	
	if (!strcmp(target_policy,"bypass")) {
		request_secure_resource = false; 
	} else if (!strcmp(target_policy,"discard")) {
		P_INFO("Target resource discarded");
		config_destroy(cf);
		cleanup();
		return 0;
	}

	int nq = 0;
	while (nq < num_queries) {
		nq++;
	
		if (request_secure_resource) {
		
			/* send OSCORE request*/
			//request_secure_resource = false;
			protected_pdu->reset();
			protected_pdu->setVersion(1);
			protected_pdu->setType(CoapPDU::COAP_CONFIRMABLE);
			protected_pdu->setCode(CoapPDU::COAP_GET);
			protected_pdu->setToken((uint8_t *)&(++token),
						sizeof(token));
			protected_pdu->setURI((char *)coap_uri.path.s, coap_uri.path.length);
			protected_pdu->setMessageID(mid1++);
			protected_pdu->setPayload(request_payload,
						  sizeof(request_payload));

			if (protected_pdu->validate()) {
				P_INFO("=================================================");
				P_INFO("CoAP message to be protected with OSCORE to resource: %s",coap_uri.path.s);
				protected_pdu->printHuman();
			}

			r = coap2oscore(protected_pdu->getPDUPointer(),
					(uint16_t)protected_pdu->getPDULength(),
					buf_oscore, &buf_oscore_len, &c_client);
			if (r != ok) {
				P_ERR("Error in coap2oscore (Error code %d)!",r);
			}

			sendto(sockfd, buf_oscore, buf_oscore_len, 0,
			       (const struct sockaddr *)&servaddr,
			       sizeof(servaddr));

			/* receive */
			n = recvfrom(sockfd, (char *)buffer, MAXLINE,
				     MSG_WAITALL, (struct sockaddr *)&servaddr,
				     &len);
			if (n < 0) {
				P_INFO("no response received from resource: %s",coap_uri.path.s);
			} else {
				r = oscore2coap((uint8_t *)buffer, n,
						coap_rx_buf, &coap_rx_buf_len,
						&oscore_flag, &c_client);

				if (r != ok) {
					P_ERR("Error in oscore2coap (Error code %d)!",r);
				}
				recvPDU = new CoapPDU((uint8_t *)coap_rx_buf,
						      coap_rx_buf_len);
				if (recvPDU->validate()) {
					P_INFO("===================================================");
					P_INFO("Response CoAP message from resource: %s", coap_uri.path.s);
					recvPDU->printHuman();
				}
			}

		} else {
			/* send CoAP request*/
			//request_secure_resource = true;
			unprotected_pdu->reset();
			unprotected_pdu->setVersion(1);
			unprotected_pdu->setType(CoapPDU::COAP_CONFIRMABLE);
			unprotected_pdu->setCode(CoapPDU::COAP_GET);
			unprotected_pdu->setToken((uint8_t *)&(++token),
						  sizeof(token));
			//unprotected_pdu->setURI((char *)"tv2", 3);
			protected_pdu->setURI((char *)coap_uri.path.s, coap_uri.path.length);
			unprotected_pdu->setMessageID(mid2++);
			unprotected_pdu->setPayload(request_payload,
						    sizeof(request_payload));

			if (unprotected_pdu->validate()) {
				P_INFO("=================================================");
				P_INFO("Unprotected CoAP message to resource: %s", coap_uri.path.s);
				unprotected_pdu->printHuman();
			}

			sendto(sockfd, unprotected_pdu->getPDUPointer(),
			       unprotected_pdu->getPDULength(), 0,
			       (const struct sockaddr *)&servaddr,
			       sizeof(servaddr));

			/* receive */
			n = recvfrom(sockfd, (char *)buffer, MAXLINE,
				     MSG_WAITALL, (struct sockaddr *)&servaddr,
				     &len);

			if (n < 0) {
				P_INFO("no response received from resource: %s", coap_uri.path.s);
			} else {
				recvPDU = new CoapPDU((uint8_t *)buffer, n);
				if (recvPDU->validate()) {
					P_INFO("=============================================");
					P_INFO("Response CoAP message from resource: %s", coap_uri.path.s);
					recvPDU->printHuman();
				}
			}
		}
		/*wait 5 sec before sending the next packet*/
		sleep(5);
	}
	
	config_destroy(cf);
	cleanup();
	close(sockfd);
	return 0;
}
