/*
 * Copyright (c) 2018 Gabriel López <gabilm@um.es>, Rafael Marín <rafa@um.es>, Fernando Pereñiguez <fernando.pereniguez@cud.upct.es> 
 *
 * This file is part of cfgipsec2.
 *
 * cfgipsec2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cfgipsec2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>


#include <sys/socket.h>
#include <sys/types.h>

#include <netinet/in.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <inttypes.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>

//#include "sysrepo.h"
//#include "sysrepo/values.h"

#include "mbedtls/base64.h"


#define MAX_LEN 2000

size_t convert_hex_to_unsigned_char(unsigned char *dest, size_t count, const char *src);
size_t convert_hex_to_uint8(uint8_t *dest, size_t count, const char *src);
void rand_str(char *dest, size_t length);
void remove_all_chars(char* str, char c);
char *decode_from_b64(char *input);
char *strremove(char *str, const char *sub);






